!> @file m_salmon.f90
!! The FishMet Model: Species-specific procedures for the Atlantic salmon.

! ------------------------------------------------------------------------------
! Copyright notice:
!   Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad
! FishMet model code is distributed according to GNU AGPL v3, see LICENSE.md
! ------------------------------------------------------------------------------

!> This module defines default equations and certain other parameters for
!! salmon species that are not practical to place into the main configuration
!! file `parameters.cfg`.
module DEF_SALMON

  use REALCALC
  use COMMONDATA
  implicit none

  !> Standardized feed pellet density, g/l, this is used instead of
  !! real feed data
  real(SRP), private, parameter :: SALMON_FEED_DENSITY_LOC = 650.0_SRP

  !> Default Fulton K factor of the fish (@f[ K = \frac{100 W}{ L^{3} } @f]),
  !! used in ::salmon_approx_length().
  real(SRP), private, parameter :: SALMON_K_FACTOR_DEF = 1.4_SRP

  contains

  !> Calculate stomach volume (ml) based on fish body mass (g),
  !! from Pirhonen, J. & Koskela, J. (2005) Indirect estimation of stomach
  !!      volume of rainbow trout Oncorhynchus mykiss (Walbaum).
  !!      Aquac.Res.36, 851-856, doi:10.1111/j.1365-2109.2005.01293.x
  elemental function salmon_stomach_volume(fish_mass) result (val_out)
    !> the body mass of the fish
    real(SRP), intent(in) :: fish_mass
    real(SRP) :: val_out

    val_out = 0.034_SRP * fish_mass + 9.33_SRP

  end function salmon_stomach_volume

  !> Calculate stomach mass capacity (g) based on fish body mass (g)
  !! Validation:with feed_density = 650 g/l, stomach_capacity(650) = 20.4295 g
  elemental function  salmon_stomach_capacity(fish_mass, feed_density)        &
                                                                result(val_out)
    !> the body mass of the fish
    real(SRP), intent(in) :: fish_mass
    !> Opional feed density to back-calculate capacity from the volume
    real(SRP), intent(in), optional :: feed_density
    real(SRP) :: val_out

    real(SRP) :: feed_density_loc

    if (present(feed_density)) then
      feed_density_loc = feed_density
    else
      feed_density_loc = SALMON_FEED_DENSITY_LOC
    end if

    val_out = salmon_stomach_volume(fish_mass) * 0.001 * feed_density_loc

  end function salmon_stomach_capacity

  !> Calculate midgut mass capacity from stomach capacity
  elemental function salmon_midgut_capacity(fish_mass, feed_density)         &
                                                              result (val_out)
    !> the body mass of the fish
    real(SRP), intent(in) :: fish_mass
    !> Opional feed density to back-calculate capacity from the volume
    real(SRP), intent(in), optional :: feed_density
    real(SRP) :: val_out

    real(SRP) :: feed_density_loc

    if (present(feed_density)) then
      feed_density_loc = feed_density
    else
      feed_density_loc = SALMON_FEED_DENSITY_LOC
    end if

    val_out  = salmon_stomach_capacity(fish_mass, feed_density_loc) * 2.3_SRP

  end function salmon_midgut_capacity

  !> calculate oxygen consumption by Atlantic salmon (Salmo salar) based on
  !! Grøttun, J.A. & Sigholt, T (1998). A model for oxygen consumption of
  !! Atlantic salmon  (Salmo salar) based on measurement of individual fish
  !! in a tunnel respirometer. Aquacultural engineering 17, 241-251.
  elemental function salmon_oxygen_consumption                                &
      (weight_kg,temperature,swimming_speed,time_unit,is_std) result (v_ox_out)
    !> Body weight of the fish, kg
    real(SRP), intent(in) :: weight_kg
    !> Temperature, Celsius
    real(SRP), intent(in) :: temperature
    !> Swimming speed of the fish, body length / s
    real(SRP), intent(in) :: swimming_speed
    !> Optional time unit, e.g. commondata::minute or commondata::hour
    !! The default value is s 1/60 of commondata:::minute
    integer, intent(in), optional :: time_unit
    !> Logical flag determining if the oxygen consumption is output in
    !! the standardised value (per kg mass) or absolute value, default is
    !! absolute value (FALSE)
    logical, intent(in), optional :: is_std

    !> Active metabolic rate, mg O2 per `time_unit`
    !! (equation default commondata::hour: O2 mg kg^-1 h^-1)
    real(SRP) :: v_ox_out

    integer :: time_unit_loc
    logical :: is_std_loc

    ! High precision representation realcalc::hrp variables to avoid
    ! `Floating-point exception - erroneous arithmetic operation` errors.
    real(HRP) :: v_ox_out_H, weight_kg_H, temperature_H, swimming_speed_H

    if (present(time_unit)) then
      time_unit_loc = time_unit
    else
      time_unit_loc = 1
    end if

    if (present(is_std)) then
      is_std_loc = is_std
    else
      is_std_loc = .FALSE.
    end if

    ! Convert to realcalc:hrp arithmetic kind for extended precision and range
    v_ox_out_H       = real(v_ox_out,       HRP)
    weight_kg_H      = real(weight_kg,      HRP)
    temperature_H    = real(temperature,    HRP)
    swimming_speed_H = real(swimming_speed, HRP)

    !> *Formula:* @f[  61.6 \times M^{-0.33} \times 1.03^{t} \times 1.79^U  @f]
    associate(V=>v_ox_out_H, BW=>weight_kg_H, T=>temperature_H, U=>swimming_speed_H)
      ! O2 mg kg^-1 h^-1
      V = 61.6_HRP * BW **(-0.33_HRP) * 1.03_HRP**T * 1.79_HRP**U
    end associate

    v_ox_out = real(v_ox_out_H, SRP)

    ! If standardised Oxygen consumption is requested (is_std=TRUE) then
    ! output the formula value as it calculates per kkg body mass,
    ! if not standardised, multiply the formula value by the body mass to
    ! get the absolute value.
    if (.NOT. is_std_loc) v_ox_out = v_ox_out * weight_kg

    ! We need to adjust the AMR according to the time_unit as the equation
    ! (with the empirical parameters) calculates per hour
    v_ox_out = time_unit_loc * v_ox_out/HOUR

  end function salmon_oxygen_consumption


end module DEF_SALMON
