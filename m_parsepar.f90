!> @file m_parsepar.f90
!! Additional module for the FishMet model: Reading plain text parameter file.

! ------------------------------------------------------------------------------
! Copyright notice: Parts of this source code are
!   Copyright (c) 2015-2024 Sergey Budaev
!   Copyright (c) 2011-2015 Pierre de Buyl
! ------------------------------------------------------------------------------

! This file is part of PARSETEXT
! Copyright 2011-2015 Pierre de Buyl
! License: BSD-3-clause
! Modified by Sergey Budaev <sbudaev@gmail.com>
!
! The following fortran 90 module helps parsing small text files for use
! as input in fortran programs.
!
! note : fixed length : length of a line -> elements et tempchar
!                     : length of the PTREAD_S function
!> This module is used to parse the configuration file.
module PARSETEXT

  use REALCALC
  use, intrinsic :: ISO_FORTRAN_ENV, only : ERROR_UNIT
  implicit none

  ! Maximumn length of the input line for parameter parsing
  integer, parameter, private :: TXT_INPUT_LEN = 144

  ! Maximum length of the file name
  integer, parameter, private :: FILENAME_LEN = 255

  ! Length of a short character string, used for comment delimniter,
  ! equal sign etc.
  integer, parameter, private :: SHORT_CHAR = 8

  ! a defined type that contains all the info from the input file
  type, public :: PTO
     private
     ! the filename
     character(len=FILENAME_LEN) :: filename
     ! the number of lines in the file, will be computed at parsing
     integer :: nlines
     ! the character used as the equality relation, defaults to '='
     character(len=SHORT_CHAR) :: equalsign
     ! the character used as the comment identifier, defaults to '#'
     character(len=SHORT_CHAR) :: comment
     ! the array containing the lines of the file; the maximum length
     ! of a line is TXT_INPUT_LEN (can be modified); elements will be
     ! allocated at parsing
     character(len=TXT_INPUT_LEN), allocatable :: elements(:)
  end type PTO

  character(len=*), parameter, private :: IGNORECHARS="c[]()/{}"
  character(len=*), parameter, private :: IGNOREPARSE = IGNORECHARS // " ,;:"
  integer, parameter, private :: MAX_PARSE = 25

  interface PTREAD_IVEC
    module procedure PTREAD_IVEC_DEFVAL
    module procedure PTREAD_IVEC_DEFVEC
  end interface PTREAD_IVEC

  interface PTREAD_RVEC
    module procedure PTREAD_DVEC_DEFVAL
    module procedure PTREAD_DVEC_DEFVEC
  end interface PTREAD_RVEC

  interface PTREAD_LVEC
    module procedure PTREAD_LVEC_DEFVAL
    module procedure PTREAD_LVEC_DEFVEC
  end interface PTREAD_LVEC

  private :: OUTSTRIP, PARSE, COMPACT, SPLIT, REMOVEBKSL

  private :: PTREAD_IVEC_DEFVAL, PTREAD_IVEC_DEFVEC,                          &
             PTREAD_DVEC_DEFVAL, PTREAD_DVEC_DEFVEC,                          &
             PTREAD_LVEC_DEFVAL, PTREAD_LVEC_DEFVEC

contains

  subroutine PTINFO(short)
    logical, intent(in), optional :: short
    logical :: short_var
    ! include 'PT_version.h':
    character(len=*), parameter :: PARSETEXT_VERSION_DATE = '09182017'
    character(len=*), parameter :: PARSETEXT_COMMIT = '2ab8aa4'
    character(len=*), parameter :: PARSETEXT_MACHINE = 'default'
    character(len=*), parameter :: PARSETEXT_COMPILER = 'default_HDF5_disable'
    character(len=*), parameter :: SVN_VERSION_STRING = "$Revision: 16074 $"
    character(len=*), parameter :: SVN_CHANGEDATE_STR =                       &
          "$LastChangedDate: 2024-07-01 16:03:41 +0200 (Mon, 01 Jul 2024) $"

    if (present(short)) then
       short_var = .true.
    else
       short_var = .false.
    end if

    if (short_var) then
       write(*,*) 'ParseText> Version/date : ',                               &
                                  trim(adjustl(PARSETEXT_VERSION_DATE))
       write(*,*) 'ParseText> SVN Revision     : ', trim(SVN_VERSION_STRING)
    else
       write(*,*) 'ParseText> Library to use human readable config files'
       write(*,*) 'ParseText> (C) 2011 P. de Buyl.'
       write(*,*) 'ParseText> Version/date : ',                               &
                                          trim(adjustl(PARSETEXT_VERSION_DATE))
       write(*,*) 'ParseText> Git commit   : ', trim(PARSETEXT_COMMIT)
       write(*,*) 'ParseText> Built on     : ', trim(PARSETEXT_MACHINE)
       write(*,*) 'ParseText> Compiler     : ', trim(PARSETEXT_COMPILER)
       write(*,*) 'ParseText> SVN Revision : ', trim(SVN_VERSION_STRING)
       write(*,*) 'ParseText> SVN Date     : ', trim(SVN_CHANGEDATE_STR)
    end if

  end subroutine PTINFO

  subroutine PTPARSE(PTin, filename, fileunit, equalsign, comment)
    type(PTO), intent(out) :: PTin
    character(len=*), intent(in) :: filename
    integer, intent(in), optional :: fileunit
    character(len=*), intent(in), optional :: equalsign, comment

    integer :: i
    integer :: iostat
    character(len=TXT_INPUT_LEN) :: tempchar

    PTin%filename = filename
    if (.not. present(fileunit)) then
       write(ERROR_UNIT, *) 'ERROR: please give a fileunit to PTPARSE'
       stop
    end if

    if (present(equalsign)) then
       PTin%equalsign = equalsign
    else
       PTin%equalsign = '='
    end if
    if (present(comment)) then
       PTin%comment = comment
    else
       PTin%comment = '#'
    end if


    open(fileunit,file=filename)
    PTin%nlines = 0
    count_loop : do
       read(fileunit, '(A)', iostat=iostat) tempchar
       if (iostat.lt.0) exit count_loop
       if (iostat.gt.0) cycle

       if (index(tempchar, PTin%comment).ne.1) then
          PTin%nlines = PTin%nlines + 1
       end if
    end do count_loop
    close(fileunit)

    if (PTin%nlines.gt.0) then
       allocate(PTin%elements(PTin%nlines))
    else
       write(ERROR_UNIT,*) 'filename = ', filename
       write(ERROR_UNIT, *) 'ERROR: No line to parse in PTPARSE'
       stop
    end if

    i = 1
    open(fileunit,file=filename)
    read_loop : do
       read(fileunit, '(A)', iostat=iostat) tempchar
       if (iostat.lt.0) exit read_loop
       if (iostat.gt.0) cycle

       if (index(tempchar, PTin%comment).ne.1) then
          PTin%elements(i) = adjustl(tempchar)
          i = i + 1
       end if
    end do read_loop
    close(fileunit)

  end subroutine PTPARSE

  subroutine PTPRINT(PTin)
    type(PTO), intent(in) :: PTin

    integer :: i

    if (allocated(PTin%elements)) then
       do i=1, PTin%nlines
          write(*,'(A)') PTin%elements(i)
       end do
    else
       write(ERROR_UNIT,*) 'WARNING: nothing to print in PTPRINT'
    end if

  end subroutine PTPRINT

  subroutine PTKILL(PTin)
    type(PTO), intent(inout) :: PTin

    if (allocated(PTin%elements)) then
       deallocate(PTin%elements)
       PTin%nlines = 0
    else
       write(ERROR_UNIT,*) 'WARNING: nothing to kill in PTKILL'
    end if
  end subroutine PTKILL

  real(HRP) function PTREAD_HR(PTin, var_name, default)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    real(HRP), intent(in), optional :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    real(HRP) :: value
    logical :: found

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
       PTREAD_HR = value
    else
       if (present(default)) then
          PTREAD_HR = default
       else
          write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                    &
                              ' not found in the file ',PTin%filename
          stop
       end if
    end if

  end function PTREAD_HR

  real(SRP) function PTREAD_R(PTin, var_name, default)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    real(SRP), intent(in), optional :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    real(SRP) :: value
    logical :: found

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
    PTREAD_R = value
    else
       if (present(default)) then
          PTREAD_R = default
       else
         write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                     &
                              ' not found in the file ',PTin%filename
         stop
       end if
    end if

  end function PTREAD_R

  integer function PTREAD_I(PTin, var_name, default)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    integer, intent(in), optional :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    integer :: value
    logical :: found

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
       PTREAD_I = value
    else
       if (present(default)) then
          PTREAD_I = default
       else
          write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                    &
                              ' not found in the file ',PTin%filename
          stop
       end if
    end if

  end function PTREAD_I

  integer(LONG) function PTREAD_LI(PTin, var_name, default) result(r)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    integer, intent(in), optional :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    integer(LONG) :: value
    logical :: found

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
       r = value
    else
       if (present(default)) then
          r = default
       else
          write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                    &
                              ' not found in the file ',PTin%filename
          stop
       end if
    end if

  end function PTREAD_LI

  character(len=TXT_INPUT_LEN) function PTREAD_S(PTin, var_name, default)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    character(len=*), optional, intent(in) :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    character(len=TXT_INPUT_LEN) :: value
    logical :: found

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
    PTREAD_S = value
    else
      if (present(default)) then
          PTREAD_S = default
      else
        write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                       &
                            ' not found in the file ',PTin%filename
        stop
      end if
    end if

  end function PTREAD_S

  logical function PTREAD_L(PTin, var_name, default)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    logical, optional, intent(in) :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    logical :: value
    logical :: found

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
    PTREAD_L = value
    else
      if (present(default)) then
          PTREAD_L = default
      else
        write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                       &
                            ' not found in the file ',PTin%filename
        stop
      end if
    end if

  end function PTREAD_L

  function PTREAD_IVEC_DEFVAL(PTin, var_name) result ( vect_out )
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    integer, allocatable, dimension(:) :: vect_out ! PTREAD_IVEC_DEFVAL(n)

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    integer, allocatable, dimension(:) :: value
    logical :: found

    character(len=TXT_INPUT_LEN), dimension(MAX_PARSE) :: parse_arr
    integer :: parse_n

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          call OUTSTRIP(tempchar, IGNORECHARS) ! Delete array delimiters.
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             call PARSE(tempchar, IGNOREPARSE, parse_arr, parse_n)
             if (.not. allocated(value) ) then
               allocate(value(parse_n))
             else
               deallocate(value)
               allocate(value(parse_n))
             end if
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    allocate(vect_out(parse_n))

    if (found) then
      vect_out = value
    else
      write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                        &
                          ' not found in the file ',PTin%filename
      stop
    end if

  end function PTREAD_IVEC_DEFVAL

  function PTREAD_IVEC_DEFVEC(PTin, var_name, default) result (vect_out)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    integer, allocatable, dimension(:) :: vect_out ! PTREAD_IVEC_DEFVEC(n)

    integer, dimension(:), intent(in) :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    integer, allocatable, dimension(:) :: value
    logical :: found

    character(len=TXT_INPUT_LEN), dimension(MAX_PARSE) :: parse_arr
    integer :: parse_n

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          call OUTSTRIP(tempchar, IGNORECHARS) ! Delete array delimiters.
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             call PARSE(tempchar, IGNOREPARSE, parse_arr, parse_n)
             if (.not. allocated(value) ) then
               allocate(value(parse_n))
             else
               deallocate(value)
               allocate(value(parse_n))
             end if
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
      allocate(vect_out(parse_n))
      vect_out = value
    else
      allocate(vect_out(size(default)))
      vect_out = default
    end if

  end function PTREAD_IVEC_DEFVEC

  function PTREAD_DVEC_DEFVAL(PTin, var_name) result (vect_out)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    real(SRP), allocatable, dimension(:) :: vect_out ! PTREAD_DVEC_DEFVAL(n)

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    real(SRP), allocatable, dimension(:) :: value
    logical :: found

    character(len=TXT_INPUT_LEN), dimension(MAX_PARSE) :: parse_arr
    integer :: parse_n

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          call OUTSTRIP(tempchar, IGNORECHARS) ! Delete array delimiters.
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             call PARSE(tempchar, IGNOREPARSE, parse_arr, parse_n)
             if (.not. allocated(value) ) then
               allocate(value(parse_n))
             else
               deallocate(value)
               allocate(value(parse_n))
             end if
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    allocate(vect_out(parse_n))

    if (found) then
      vect_out = value
    else
      write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                        &
                          ' not found in the file ',PTin%filename
      stop
    end if

  end function PTREAD_DVEC_DEFVAL

  function PTREAD_DVEC_DEFVEC(PTin, var_name, default) result (vect_out)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    real(SRP), allocatable, dimension(:) :: vect_out  ! PTREAD_DVEC_DEFVEC(n)
    real(SRP), dimension(:), intent(in) :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    real(SRP), allocatable, dimension(:) :: value
    logical :: found

    character(len=TXT_INPUT_LEN), dimension(MAX_PARSE) :: parse_arr
  	integer :: parse_n

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          call OUTSTRIP(tempchar, IGNORECHARS) ! Delete array delimiters.
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             call PARSE(tempchar, IGNOREPARSE, parse_arr, parse_n)
             if (.not. allocated(value) ) then
               allocate(value(parse_n))
             else
               deallocate(value)
               allocate(value(parse_n))
             end if
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
      allocate(vect_out(parse_n))
      vect_out = value
    else
      allocate(vect_out(size(default)))
      vect_out = default
    end if

  end function PTREAD_DVEC_DEFVEC

  function PTREAD_LVEC_DEFVAL(PTin, var_name) result (vect_out)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    logical, allocatable, dimension(:) :: vect_out  ! PTREAD_LVEC_DEFVAL(n)

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    logical, allocatable, dimension(:) :: value
    logical :: found

    character(len=TXT_INPUT_LEN), dimension(MAX_PARSE) :: parse_arr
  	integer :: parse_n

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          call OUTSTRIP(tempchar, IGNORECHARS) ! Delete array delimiters.
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             call PARSE(tempchar, IGNOREPARSE, parse_arr, parse_n)
             if (.not. allocated(value) ) then
               allocate(value(parse_n))
             else
               deallocate(value)
               allocate(value(parse_n))
             end if
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    allocate(vect_out(parse_n))

    if (found) then
      vect_out = value
    else
      write(ERROR_UNIT,*) 'ERROR: variable ',var_name,                        &
                          ' not found in the file ',PTin%filename
      stop
    end if

  end function PTREAD_LVEC_DEFVAL

  function PTREAD_LVEC_DEFVEC(PTin, var_name, default) result (vect_out)
    type(PTO), intent(in) :: PTin
    character(len=*), intent(in) :: var_name
    logical, allocatable, dimension(:) :: vect_out ! PTREAD_LVEC_DEFVEC(n)
    logical, dimension(:), intent(in) :: default

    integer :: i
    character(len=TXT_INPUT_LEN) :: tempchar
    logical, allocatable, dimension(:) :: value
    logical :: found

    character(len=TXT_INPUT_LEN), dimension(MAX_PARSE) :: parse_arr
  	integer :: parse_n

    found = .false.
    do i=1,PTin%nlines
       if (index(PTin%elements(i),var_name).eq.1) then
          tempchar = PTin%elements(i)(len(trim(var_name))+1:)
          call OUTSTRIP(tempchar, IGNORECHARS) ! Delete array delimiters.
          tempchar = adjustl(tempchar)
          if (index(tempchar,trim(PTin%equalsign)).eq.1) then
             tempchar = adjustl(tempchar)
             tempchar = tempchar(len(trim(PTin%equalsign))+1:)
             call PARSE(tempchar, IGNOREPARSE, parse_arr, parse_n)
             if (.not. allocated(value) ) then
               allocate(value(parse_n))
             else
               deallocate(value)
               allocate(value(parse_n))
             end if
             read(tempchar,*) value
             found = .true.
             exit
          end if
       end if
    end do

    if (found) then
      allocate(vect_out(parse_n))
      vect_out = value
    else
      allocate(vect_out(size(default)))
      vect_out = default
    end if

  end function PTREAD_LVEC_DEFVEC

  ! STRIP - private function copied from HEDTOOLS (`STRIP` from
  !         `BASE_STRINGS` r9437) to allow isolated use of PARSETEXT module.
  ! PURPOSE: removes (strips) a set of characters from a string
  !
  ! CALL PARAMETERS:
  !    Character string to be stripped
  !    Character string containing the set of characters to be stripped
  !
  ! Author: Rosetta code
  ! https://www.rosettacode.org/wiki/Strip_a_set_of_characters_from_a_string#Fortran
  elemental subroutine OUTSTRIP(string,set)

    character(len=*), intent(inout) :: string
    character(len=*), intent(in)    :: set
    integer                         :: old, new, stride

    old = 1; new = 1
    do
      stride = scan( string( old : ), set )
      if ( stride > 0 ) then
        string( new : new+stride-2 ) = string( old : old+stride-2 )
        old = old+stride
        new = new+stride-1
      else
        string( new : ) = string( old : )
        return
      end if
    end do
  end subroutine OUTSTRIP

  ! NOTE: The procedures below are taken from the STRINGS module they are
  !       copied to avoid STRINGS module being a dependency for PARSETEXT.

  ! Parses the string 'str' into arguments args(1), ..., args(nargs) based on
  ! the delimiters contained in the string 'delims'. Preceding a delimiter in
  ! 'str' by a backslash (\) makes this particular instance not a delimiter.
  ! The integer output variable nargs contains the number of arguments found.
  subroutine PARSE(str,delims,args,nargs)

    character(len=*) :: str,delims
    character(len=len_trim(str)) :: strsav
    character(len=*),dimension(:) :: args

    integer :: nargs, i, k, lenstr, na

    strsav=str
    call COMPACT(str)
    na=size(args)
    do i=1,na
      args(i)=' '
    end do
    nargs=0
    lenstr=len_trim(str)
    if(lenstr==0) return
    k=0

    do
       if(len_trim(str) == 0) exit
       nargs=nargs+1
       call SPLIT(str,delims,args(nargs))
       call REMOVEBKSL(args(nargs))
    end do
    str=strsav

  end subroutine PARSE

  ! Converts multiple spaces and tabs to single spaces; deletes control characters;
  ! removes initial spaces.
  subroutine COMPACT(str)

    character(len=*):: str
    character(len=1):: ch
    character(len=len_trim(str)):: outstr

    integer :: i, k, ich, isp, lenstr

    str=adjustl(str)
    lenstr=len_trim(str)
    outstr=' '
    isp=0
    k=0

    do i=1,lenstr
      ch=str(i:i)
      ich=iachar(ch)

      select case(ich)

        case(9,32)     ! space or tab character
          if(isp==0) then
            k=k+1
            outstr(k:k)=' '
          end if
          isp=1

        case(33:)      ! not a space, quote, or control character
          k=k+1
          outstr(k:k)=ch
          isp=0

      end select

    end do

    str=adjustl(outstr)

  end subroutine COMPACT

  ! Routine finds the first instance of a character from 'delims' in the
  ! the string 'str'. The characters before the found delimiter are
  ! output in 'before'. The characters after the found delimiter are
  ! output in 'str'. The optional output character 'sep' contains the
  ! found delimiter. A delimiter in 'str' is treated like an ordinary
  ! character if it is preceded by a backslash (\). If the backslash
  ! character is desired in 'str', then precede it with another backslash.
  subroutine SPLIT(str,delims,before,sep)

    character(len=*) :: str,delims,before
    character,optional :: sep
    logical :: pres
    character :: ch,cha

    integer :: i, k, ipos, iposa, ibsl, lenstr

    pres=present(sep)
    str=adjustl(str)
    call compact(str)
    lenstr=len_trim(str)
    if(lenstr == 0) return        ! string str is empty
    k=0
    ibsl=0                        ! backslash initially inactive
    before=' '
    do i=1,lenstr
       ch=str(i:i)
       if(ibsl == 1) then          ! backslash active
          k=k+1
          before(k:k)=ch
          ibsl=0
          cycle
       end if
       if(ch == '\') then          ! backslash with backslash inactive
          k=k+1
          before(k:k)=ch
          ibsl=1
          cycle
       end if
       ipos=index(delims,ch)
       if(ipos == 0) then          ! character is not a delimiter
          k=k+1
          before(k:k)=ch
          cycle
       end if
       if(ch /= ' ') then          ! character is a delimiter that is not a space
          str=str(i+1:)
          if(pres) sep=ch
          exit
       end if
       cha=str(i+1:i+1)            ! character is a space delimiter
       iposa=index(delims,cha)
       if(iposa > 0) then          ! next character is a delimiter
          str=str(i+2:)
          if(pres) sep=cha
          exit
       else
          str=str(i+1:)
          if(pres) sep=ch
          exit
       end if
    end do
    if(i >= lenstr) str=''
    str=adjustl(str)              ! remove initial spaces
    return

  end subroutine SPLIT

  ! Removes backslash (\) characters. Double backslashes (\\) are replaced
  ! by a single backslash.
  subroutine REMOVEBKSL(str)

    character(len=*):: str
    character(len=1):: ch
    character(len=len_trim(str))::outstr

    integer :: i, k, ibsl, lenstr

    str=adjustl(str)
    lenstr=len_trim(str)
    outstr=' '
    k=0
    ibsl=0                        ! backslash initially inactive

    do i=1,lenstr
      ch=str(i:i)
      if(ibsl == 1) then          ! backslash active
       k=k+1
       outstr(k:k)=ch
       ibsl=0
       cycle
      end if
      if(ch == '\') then          ! backslash with backslash inactive
       ibsl=1
       cycle
      end if
      k=k+1
      outstr(k:k)=ch              ! non-backslash with backslash inactive
    end do

    str=adjustl(outstr)

  end subroutine REMOVEBKSL


end module PARSETEXT
