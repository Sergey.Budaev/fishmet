!> @file m_runtime.f90
!! The FishMet Model: Parameter input and output, model scheduling.

! ------------------------------------------------------------------------------
! Copyright notice:
!   Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad
! FishMet model code is distributed according to GNU AGPL v3, see LICENSE.md
! ------------------------------------------------------------------------------

! Notes:
!         This module is for general scheduling of the model, parameters
!         inpuit and output

!> Module that defines the runtime behaviour of the model
module RUNTIME

  use REALCALC
  use COMMONDATA
  use PARSETEXT
  use BASE_STRINGS, only : LOWERCASE, PARSE
  use SIMULATE
  use THE_FISH
  use, intrinsic :: ISO_FORTRAN_ENV, only : ERROR_UNIT

  implicit none

  contains

  !> This procedure performs basic initialisation of the system:
  !!  - read environment variables determining global behaviour of the model
  !!  - call ::read_parameters_file() to get global model parameters from the
  !!    configuration file
  !! .
  impure subroutine system_init
    use STUBS, only : print_help_switches

    character(len=LABEL_LEN) :: t_string

    integer :: i, n_args
    character(LABEL_LEN), dimension(:), allocatable :: cmd_args

    !> ### Command line parameters and environment variables ###
    !> The behaviour of the model program can be controlled using command line
    !! parameters and environment variables.
    !    Get the number of command line arguments, they will be placed
    !    into the `cmd_args array`.
    n_args = command_argument_count()
    allocate(cmd_args(n_args))

    !    Parse command line arguments.
    do i=1, n_args
      call get_command_argument(number = i, value = cmd_args(i))
      cmd_args(i) = LOWERCASE(cmd_args(i))
    end do

    !> #### Help screen ####
    !> Help screen: if the program is executed with one of these command line
    !! parameters `-h --help -help /h /help`, a short screen help is
    !! printed on the terminal and the program halts.
    if ( any(cmd_args=="-h") .or.                                             &
         any(cmd_args=="--help") .or.                                         &
         any(cmd_args=="-help") .or.                                          &
         any(cmd_args=="/h") .or.                                             &
         any(cmd_args=="/help") )  then
      call print_help_switches()
      stop
    end if

    !> #### Set the output mode ####
    !> There are two output modes: normal and "quiet," in the later case
    !! only requested information is written to the screen, no accessory
    !! information is written.
    if ( any(cmd_args=="-q") .or. any(cmd_args=="--quiet") .or.               &
         any(cmd_args=="/q") .or. any(cmd_args=="/quiet") ) then
      Verbose = .FALSE.
    else
      Verbose = .TRUE.
    end if

    !> #### Parameters file ####
    !! The main parameter file is kept in commondata::global_param_file_name
    !! and is defined by the environment variable `FFA_MODEL_PARAMETER_FILE`.
    call get_environment_variable( name="FFA_MODEL_PARAMETER_FILE",           &
                                   value=t_string)
    if ( len_trim(t_string)==0 ) then
      Global_Param_File_Name = PARAM_FILE_NAME_DEF
    else
      Global_Param_File_Name = t_string
    end if


    !> The environment variable  `FFA_MODEL_OUTPUT_DEST` can set the output
    !! destination directory in addition to the `output_dest` parameter in the
    !! model configuration file. The value set by the environment variable
    !! takes priority over the configuration file.
    call get_environment_variable( name="FFA_MODEL_OUTPUT_DEST",              &
                                   value=t_string)
    if ( len_trim(t_string)==0 ) then
      Global_Output_Dest = OUTPUT_DEST_DIR
    else
      Global_Output_Dest = t_string
    end if

    !> - Read global parameters from the configuration file defined in
    !!   commondata::param_file_name_def.
    !! .
    call read_parameters()

  end subroutine system_init

  !> This subroutine starts and schedules the model
  impure subroutine system_run_main()
    use STUBS, only : cmd_iface_process_commands
    use CSV_IO, only : GET_FREE_FUNIT

    logical :: is_exit_program

    integer :: stats_file_unit, file_error_status
    logical :: is_model_stats_save_header_done, file_exists

    !> - Print param,eters
    write(*,"(a)") model_parameters_txt()

    !> - Run the model
    call test_run()

    !> - Print basic output stats
    write(*,"(a)") model_output_stats_txt()

    call output_arrays_save_csv( OUTPUT_ARRAYS_CSV_FILE // ".csv" )
    call output_arrays_save_rate_data_csv( OUTPUT_RATE_DATA_CSV_FILE // ".csv")

    !> - Save model stats row
    is_model_stats_save_header_done = .FALSE.

    inquire( file=Global_Stats_Output_File, exist=file_exists )
    if (file_exists) then
      is_model_stats_save_header_done = .TRUE.
    end if

    if (is_model_stats_save_header_done) then
      if (Verbose) write(*,*) "NOTE: File ", trim(Global_Stats_Output_File),  &
                                            " exists, data will be appended."
      ! not saved header yet, equivalent to `append stats` command
      stats_file_unit = GET_FREE_FUNIT()
      open(stats_file_unit, file=Global_Stats_Output_File,                    &
            status="old", position="append", action="write",                  &
            iostat=file_error_status)
      if (file_error_status /= 0) then
        write(ERROR_UNIT,*)                                                   &
          ">>> ERROR: file write error! Use `output statistics` " //          &
          "command first, check free space or permissions."
      else
        write(stats_file_unit,"(a)") model_output_stats_row_csv()
      end if
      close(stats_file_unit)
    else
      ! new file started, equivalent `output stats` command
      stats_file_unit = GET_FREE_FUNIT()
      open(stats_file_unit, file=Global_Stats_Output_File,                    &
                status="replace", action="write", iostat=file_error_status)
      if (file_error_status /= 0) then
        write(ERROR_UNIT,*)                                                   &
          ">>> ERROR: file write error! Use `output statistics` " //          &
          "command first, check free space or permissions."
      else
        write(stats_file_unit,"(a)")                                          &
                        model_output_stats_row_csv(show_header=.TRUE.)
      end if
      close(stats_file_unit)
    end if

  end subroutine system_run_main

  !> The subroutine **read_parameters** reads global parameters
  !! defined in the commondata module from configuration file
  !! commondata::param_file_name_def.
  impure subroutine read_parameters()
    use CSV_IO, only : GET_FREE_FUNIT, FS_UNLINK

    integer, parameter :: PARAM_FUNIT = 254   ! File unit for config file
    type(PTO) :: cf                           ! PARSETEXT file handle object
    logical :: config_file_exist, config_param_is_missing
    logical :: iface_is_gui, is_scale_relative
    integer :: iin, f_unit_tmp, iostat_f_tmp
    character(len=:), allocatable :: tmp_parse
    character(len=*), parameter :: F_NAME_TMP = ".tmpfile"

    ! a temporary copy of Global_Transport_Pattern_T for analysis
    real(SRP), allocatable, dimension(:):: get_transport_pattern_t

    ! .......................................................................
    ! Check if the configuration file exists, if not, exit with error.
    inquire( file=trim(Global_Param_File_Name), exist=config_file_exist )
    if (.not. config_file_exist ) then
      call exit_with_error( "Configuration file '" //                         &
                    trim(Global_Param_File_Name) // "' cannot be found." )
    end if

    ! .......................................................................
    ! Open and parse the input configuration file
    call PTPARSE(cf, trim(Global_Param_File_Name), PARAM_FUNIT)

    ! Read the parameters
    ! ### All following are model parameters

    Global_Run_Model_Hours = PTREAD_I(cf, 'run_model_hours', UNKNOWN)

    Global_Day_Starts_Hour = PTREAD_I(cf, 'day_starts_hour', 0)

    Global_Run_Model_Feed_Offset = PTREAD_I(cf, 'feed_start_offset', 0)

    Global_Hours_Daytime_Feeding = PTREAD_I(cf, 'daytime_hours', UNKNOWN)

    Global_Baseline_Activity_Day =                                            &
                              PTREAD_R(cf, 'baseline_activity_day', MISSING)

    Global_Baseline_Activity_Night =                                          &
                              PTREAD_R(cf, 'baseline_activity_night', MISSING)

    Global_Body_Mass = PTREAD_R(cf, 'body_mass', MISSING)

    Global_Stomach_Mass = PTREAD_R(cf,'stomach_capacity', MISSING)

    Global_MidGut_Mass = PTREAD_R(cf,'midgut_capacity', MISSING)

    Global_Absorption_Ratio = PTREAD_R(cf,'absorption_ratio', MISSING)

    Global_Ingestion_Delay_Min = PTREAD_I(cf, 'ingestion_delay', UNKNOWN)

    Global_Water_Uptake = PTREAD_R(cf,'water_uptake', MISSING)

    Global_Water_Uptake_A = PTREAD_R(cf,'water_uptake_a', MISSING)

    Global_Water_Uptake_R = PTREAD_R(cf,'water_uptake_r', MISSING)

    Global_Digestion_Delay_Min = PTREAD_I(cf, 'digestion_delay', UNKNOWN)

    Global_Maximum_Duration_Midgut_Min=PTREAD_I(cf, 'midgut_maxdur', UNKNOWN)

    Global_Appetite_Fish_Night = PTREAD_R(cf, "appetite_night", MISSING)

    Global_Appetite_Stomach_Threshold =                                       &
                            PTREAD_R(cf,'appetite_threshold_stomach', MISSING)

    Global_Appetite_Logist_A = PTREAD_R(cf,'appetite_factor_a', MISSING)

    Global_Appetite_Logist_R = PTREAD_R(cf,'appetite_factor_r', MISSING)

    Global_Appetite_Activity_Factor =                                         &
                              PTREAD_R(cf,'activity_appetite_factor', 0.3_SRP)

    Global_UE_ZE_Factor = PTREAD_R(cf, 'branchial_energy_factor', MISSING)

    Global_UE_ZE_Ammonia_Excretion =                                          &
                              PTREAD_R(cf, 'branchial_ammonia_rate', MISSING)

    if ( Global_UE_ZE_Factor == MISSING .and.                                 &
         Global_UE_ZE_Ammonia_Excretion > 0.0_SRP ) then
      Global_IS_UE_ZE_Fixed_Rate = .TRUE.
      if (IS_DEBUG .and. Verbose) write(*,"(a,f6.4,a)")                       &
                    "NOTE: branchial and urinal energy loss is fixed rate: ", &
                    Global_UE_ZE_Ammonia_Excretion, " mu mol/g/h"
    else if ( Global_UE_ZE_Factor == MISSING .and.                            &
              Global_UE_ZE_Ammonia_Excretion == MISSING ) then
      Global_UE_ZE_Factor = 0.0_SRP
      Global_IS_UE_ZE_Fixed_Rate   = .FALSE.
      if (IS_DEBUG .and. Verbose)                                             &
          write(*,*) "NOTE: branchial and urinal energy loss zero"
    else
      if (Global_UE_ZE_Factor < 0.0_SRP) Global_UE_ZE_Factor = 0.0_SRP
      Global_IS_UE_ZE_Fixed_Rate   = .FALSE.
    end if

    Global_SDA_Absorp_Rate_Max =                                              &
                PTREAD_R(cf, 'sda_absorption_rate_max', 0.0_SRP )

    Global_SDA_Factor_Max  = PTREAD_R(cf, 'sda_energy_factor_max', 0.0_SRP)

    ! Get Global_Transport_Pattern_T and autodetect time scale, s or h
    get_transport_pattern_t = PTREAD_RVEC( cf, 'transport_pattern_t' )

    ! Check if the time scale of the stomach transfer pattern vector is
    ! provided in hours or in raw data sec. The main parameter
    ! commondata::global_transport_pattern_t is in sec.
    CHECK_SCALE: if ( get_transport_pattern_t(2) > 120.0_SRP ) then
      ! - large number is provided, means the parameter vector is given
      !   in raw seconds.
      Global_Transport_Pattern_T = nint(get_transport_pattern_t)
      if (IS_DEBUG .and. Verbose)                                             &
                write(*,*) "NOTE: transport_pattern_t in raw sec"
    else CHECK_SCALE
      ! - small numbers (<120 = 2 min) means the parameter array is
      !   given in hours
      Global_Transport_Pattern_T = nint(get_transport_pattern_t*HOUR)
      if (IS_DEBUG  .and. Verbose)                                            &
                write(ERROR_UNIT,"(a)") "NOTE: transport_pattern_t in hours"
    end if CHECK_SCALE

    Global_Transport_Pattern_R = PTREAD_RVEC( cf, 'transport_pattern_r' )

    if (size(Global_Transport_Pattern_T)/=size(Global_Transport_Pattern_R))   &
      call exit_with_error( "transport_pattern_t, transport_pattern_r" //     &
                  " must have same dimensions", no_exit=.FALSE. )

    Global_Transport_Dim = size(Global_Transport_Pattern_T)

    Global_Transport_Baseline_Temperature =                                   &
                            PTREAD_R(cf,'transport_pattern_base_temp', MISSING)

    Global_Transport_Baseline_Fish_Mass =                                     &
                            PTREAD_R(cf,'transport_pattern_base_mass', MISSING)

    Global_Mid_Gut_MM_r_max = PTREAD_R(cf,'midgut_michaelis_r_max', MISSING)

    Global_Mid_Gut_MM_K_M = PTREAD_R(cf,'midgut_michaelis_k', MISSING)

    Global_Energy_Appetite_Rate = PTREAD_R(cf,"appetite_energy_rate",40.0_SRP)

    Global_Energy_Appetite_Shift = PTREAD_R(cf,"appetite_energy_shift",0.2_SRP)

    Global_Oxygen_Grid_X_Temp = PTREAD_RVEC( cf, 'smr_oxygen_temp',           &
                                             TROUT_OXYGEN_GRID_X_TEMP)

    Global_Oxygen_Grid_Y_O2std = PTREAD_RVEC( cf, 'smr_oxygen_o2',            &
                                              TROUT_OXYGEN_GRID_Y_O2STD)

    if (size(Global_Oxygen_Grid_X_Temp)/=size(Global_Oxygen_Grid_Y_O2std))    &
      call exit_with_error( "smr_oxygen_temp, smr_oxygen_o2" //               &
                  " must have same dimensions", no_exit=.FALSE. )

    Global_Food_Item_Mass = PTREAD_R(cf,'food_item_mass', MISSING)

    Global_Food_Gross_Energy = PTREAD_R(cf,'feed_gross_energy', MISSING)

    Global_Temperature = PTREAD_R(cf, 'temperature', MISSING)

    Global_Temp_Factor_Midgut_t = PTREAD_RVEC(cf,'midgut_temp_fact_t',        &
                                                [1.0_SRP, 10.0_SRP, 25.0_SRP])

    Global_Temp_Factor_Midgut_m = PTREAD_RVEC(cf,'midgut_temp_fact_m',        &
                                                [1.0_SRP, 1.0_SRP, 1.0_SRP])

    Global_Food_Input_Rate = PTREAD_R(cf,'food_input_rate', MISSING)

    Global_Stomach_Midgut_Mass_Is_Automatic = PTREAD_L( cf,                   &
                                        'stomach_midgut_automatic', .FALSE. )

    Global_Interval_Food_Param = PTREAD_IVEC( cf, 'food_provision_pattern' )

    Global_Food_Pattern_File = PTREAD_S( cf, "food_provision_file_name",  "")

    Global_Food_Pattern_File_Is_Propagate = PTREAD_L( cf,                     &
                                        'food_provision_file_repeat', .TRUE. )

    Global_Food_Pattern_File_Is_Steps = PTREAD_L( cf,                     &
                                      'food_provisioning_file_by_s', .FALSE. )

    Global_Rate_Interval = PTREAD_I(cf, 'rate_interval', 10)

    Global_Stats_Output_File = PTREAD_S( cf, "stats_output_file",             &
                                                        DEF_STATS_OUTPUT_FILE)

    Global_Output_Stats_Is_Long = PTREAD_L(cf, "stats_output_long", .TRUE.)

    Global_Stomach_Emptying_Matrix_File =                                     &
                                PTREAD_S(cf, "stomach_emptying_matrix", "")
    if ( trim(Global_Stomach_Emptying_Matrix_File) == "") then
      Global_Stomach_Emptying_Pattern = stomach_emptying_def_default()
      if (IS_DEBUG .and. Verbose) write(ERROR_UNIT,"(a)")                     &
        "WARNING: stomach emptying grid matrix is not set, use default:" //   &
        CRLF // stomach_emptying_txt()
    else
      inquire( file=trim(Global_Stomach_Emptying_Matrix_File),                &
                                                    exist=config_file_exist )
      if (.not. config_file_exist ) then
        write(ERROR_UNIT,"(a)") "WARNING: stomach emptying grid file " //     &
                     trim(Global_Stomach_Emptying_Matrix_File) // " not found"
        write(ERROR_UNIT,"(a)") "         use default matrix"
        Global_Stomach_Emptying_Matrix_File = ""
        Global_Stomach_Emptying_Pattern = stomach_emptying_def_default()
      else
        Global_Stomach_Emptying_Pattern =                                     &
          get_stomach_emptying_matrix_csv(Global_Stomach_Emptying_Matrix_File)
        if (size(Global_Stomach_Emptying_Pattern%emptying_time)<=9) then
          write(ERROR_UNIT,"(a)") "WARNING: stomach emptying grid file: " //  &
                        trim(Global_Stomach_Emptying_Matrix_File)
          write(ERROR_UNIT,"(a)")                                             &
                         "         does not seem to contain correct data, "// &
                        "use default matrix"
          Global_Stomach_Emptying_Pattern = stomach_emptying_def_default()
          Global_Stomach_Emptying_Matrix_File = ""
        end if

      end if
    end if

    Global_Stress_Factor_Hour = PTREAD_RVEC(cf,                               &
                                "stress_grid_hour", [MISSING, MISSING])
    Global_Stress_Fact_Suppress = PTREAD_RVEC(cf,                             &
                                "stress_grid_fact", [MISSING, MISSING])

    Global_Stress_Intervention_Is_Minutes = PTREAD_L(cf,"stress_is_min",.TRUE.)

    Global_Stress_Cost_SMR = PTREAD_R(cf, "stress_metabolic_cost", MISSING)

    Global_Stress_Activity_Decr = PTREAD_R(cf, "stress_inactivity", MISSING)

    if ( size(Global_Stress_Factor_Hour) /=                                   &
         size(Global_Stress_Fact_Suppress) )  then
      Global_Stress_Factor_Hour = [MISSING, MISSING]
      Global_Stress_Fact_Suppress = [MISSING, MISSING]
      write(ERROR_UNIT,"(a)")                                                 &
                    "WARNING: Stress grid arrays 'stress_grid_hour' and"
      write(ERROR_UNIT,"(a)")                                                 &
                    "         'stress_grid_fact' non-conformant, " //  &
                    "stress disabled"
    end if

    ! Get Global_Stress_Intervention_Time from the parameter file in correct
    ! units. Note that Global_Stress_Intervention_Is_Minutes (stress_is_min)
    ! can appear in the parameter file before or after stress because the file
    ! is parsed after it is fully read
    GET_STRESS_ARRAY: block
      integer(LONG), allocatable, dimension(:) :: stress_timing_in
      stress_timing_in = PTREAD_IVEC(cf, "stress", [UNKNOWN])
      if ( any(stress_timing_in==UNKNOWN .or. any(stress_timing_in<0)) ) then
        Global_Stress_Intervention_Time = [UNKNOWN]
      else
        if (Global_Stress_Intervention_Is_Minutes) then
          Global_Stress_Intervention_Time = stress_timing_in * MINUTE
        else
          Global_Stress_Intervention_Time = stress_timing_in
        end if
      end if
    end block GET_STRESS_ARRAY

    ! Destination output directory commondata::global_output_dest is set from
    ! the configuration file only if it is not already set to some non-default
    ! value from the environment variable `FFA_MODEL_OUTPUT_DEST`
    ! (see system_init()).
    if ( Global_Output_Dest == OUTPUT_DEST_DIR ) then
      Global_Output_Dest = PTREAD_S( cf, "output_dest", OUTPUT_DEST_DIR )
    end if

    ! Check the above set output directory is writeable
    CHECK_WRITE_DIR: if (Global_Output_Dest /= OUTPUT_DEST_DIR) then
      f_unit_tmp = GET_FREE_FUNIT()
      open( unit=f_unit_tmp, file=trim(Global_Output_Dest) // F_NAME_TMP,     &
            status='replace', iostat=iostat_f_tmp )
      if (iostat_f_tmp /= 0) then
        if (IS_DEBUG .and. Verbose) write(ERROR_UNIT,"(a)")                   &
              "WARNING: destination directory " // trim(Global_Output_Dest) //&
              " is NOT writeable, reset to default."
        Global_Output_Dest = OUTPUT_DEST_DIR
      end if
      close(f_unit_tmp)
      call FS_UNLINK(trim(Global_Output_Dest) // F_NAME_TMP)
    end if CHECK_WRITE_DIR

    call PTKILL(cf)

    ! .......................................................................
    ! Check the configuration data, the file must always provide all
    ! data, missing values are not allowed

    config_param_is_missing = .FALSE.

    if (Global_Baseline_Activity_Day == MISSING) then
      call exit_with_error("baseline_activity_day is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if (Global_Baseline_Activity_Night == MISSING) then
      call exit_with_error("baseline_activity_night is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if (Global_Body_Mass == MISSING) then
      call exit_with_error("body_mass is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if ( Global_Stomach_Mass == MISSING .OR.                                  &
         Global_Stomach_Midgut_Mass_Is_Automatic ) then
      Global_Stomach_Mass = salmon_stomach_capacity(Global_Body_Mass)
      if (IS_DEBUG .and. Verbose) write(ERROR_UNIT, "(a,f9.3)")               &
        "WARNING: FILE_PARAMETERS: stomach_capacity from body_mass:",         &
        Global_Stomach_Mass
    end if

    if ( Global_MidGut_Mass == MISSING .or.                                   &
         Global_Stomach_Midgut_Mass_Is_Automatic ) then
      Global_MidGut_Mass = salmon_midgut_capacity(Global_Body_Mass)
      if (IS_DEBUG .and. Verbose) write(ERROR_UNIT, "(a,f9.3)")               &
        "WARNING: FILE_PARAMETERS: midgut_capacity from body_mass:",          &
        Global_MidGut_Mass
    end if

    if (Global_Ingestion_Delay_Min == UNKNOWN) then
      call exit_with_error("ingestion_delay is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if (Global_Water_Uptake == MISSING) then
      call exit_with_error("water_uptake is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if ( all(Global_Transport_Pattern_T == UNKNOWN) ) then
      call exit_with_error("transport_pattern_t is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if ( all(Global_Transport_Pattern_R == MISSING) ) then
      call exit_with_error("transport_pattern_r is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if ( all(Global_Oxygen_Grid_X_Temp == UNKNOWN) ) then
      Global_Oxygen_Grid_X_Temp  = TROUT_OXYGEN_GRID_X_TEMP
      call exit_with_error("smr_oxygen_temp is MISSING", no_exit=.TRUE.)
    end if

    if ( all(Global_Oxygen_Grid_Y_O2std == UNKNOWN) ) then
      Global_Oxygen_Grid_Y_O2std = TROUT_OXYGEN_GRID_Y_O2STD
      call exit_with_error("smr_oxygen_o2 is MISSING", no_exit=.TRUE.)
    end if

    if (Global_Food_Item_Mass == MISSING) then
      call exit_with_error("food_item_mass is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if (Global_Temperature == MISSING) then
      call exit_with_error("temperature is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    if (Global_Food_Input_Rate == MISSING) then
      call exit_with_error("food_input_rate is MISSING", no_exit=.TRUE.)
      config_param_is_missing = .TRUE.
    end if

    ! Final check if any of the above configuration parameters was not set
    ! correctly, if so, halt with error
    if (config_param_is_missing)                                              &
      call exit_with_error( "Bad configuration file: " //                     &
                            trim(Global_Param_File_Name) )

  end subroutine read_parameters

  !> A wrapper subroutine that will produce error message and halt
  impure subroutine exit_with_error(message, no_exit)
    use STUBS, only : exit_with_error_cmd

    !> The error message that is printed to the standard error device or
    !! appear as a warning window depending on the interface type.
    character(len=*), intent(in) :: message
    !> Optional parameter that dictates NOT to stop the program and just
    !! print the error message. This may be useful if the program must
    !! check a series of conditions and then exit at the last one.
    logical, optional, intent(in) :: no_exit

    ! Local copy of optional parameter
    logical :: no_exit_loc

    if (present(no_exit)) then
      no_exit_loc = no_exit
    else
      no_exit_loc = .FALSE.
    end if

    call exit_with_error_cmd(message, no_exit_loc)

  end subroutine exit_with_error

  !-----------------------------------------------------------------------------
  !> Parse and cut revision **number** in form of string from the whole SVN
  !! revision string. SVN revision number can therefore be included into the
  !! model outputs and output file names. This is convenient because the model
  !! version is identified by a single SVN revision number.
  !! @returns  revision number from Subversion.
  !! @warning `STRINGS` module uses unsafe coding prone to bugs, e.g.
  !!           does not clearly state dummy parameters intent and doesn't
  !!           work correctly with `parameter`s.
  function parse_svn_version ( svn_version_string ) result (sversion)
    !> The standard revision string as defined by the `$Revision: XXX $`
    !! keyword from Subversion.
    character(*), intent(in) :: svn_version_string
    character(len=:), allocatable :: sversion ! @returns revision number.

    ! @note Have to copy `svn_version_string` to this local variable as
    !       `PARSE` is broken when used with fixed parameters (no `intent`).
    character(len=len(svn_version_string)) :: svn_string_copy
    ! Delimiters for substrings.
    character(len=3) :: delims = " :$"
    ! String parts after parsing and cutting. Are 3 parts enough?
    character(len=len(svn_version_string)), dimension(3) :: sargs
    integer :: n_args

    !> ### Implementation notes ###
    !> Subversion has a useful feature: various keywords can be inserted and
    !! automatically updated in the source code under revision control, e.g.
    !! revision number, date, user etc. The character string parameter constant
    !! `commondata::svn_version_string` keeps the Subversion revision tag.
    !! This subroutine parses the tag striping all other characters out.
    svn_string_copy = svn_version_string
    call PARSE(svn_string_copy, delims, sargs, n_args)
    sversion = trim(sargs(n_args))      ! Version number is the last part.

  end function parse_svn_version

  !> Check if the stomach pattern matrix agrees with the stomach emptying
  !! partameter matrix, i.e. the difference is less than one hour.
  elemental function check_stomach_transport_emptying_agree() result (is_agree)
    logical :: is_agree

    real(SRP) :: check_calc, check_last_param
    ! Absolute difference must be within this value
    real(SRP), parameter :: HOUR_DIFF_MAX = 1.0_SRP

    check_calc =                                                              &
        real( stomach_emptying_time(Global_Body_Mass, Global_Temperature), SRP)
    check_last_param =                                                        &
        real(Global_Transport_Pattern_T(size(Global_Transport_Pattern_T)),SRP)

    if (abs(check_calc / HOUR - check_last_param / HOUR) < HOUR_DIFF_MAX) then
       is_agree = .TRUE.
    else
       is_agree = .FALSE.
    end if

  end function check_stomach_transport_emptying_agree


end module RUNTIME



