# The FishMet Model

**FishMet** is a mechanistic, process-based simulation model aimed at
predicting appetite, feed intake and growth in salmonid fish.

- Budaev, S., Cusimano, G.M. & Rønnestad, I. (2024) FishMet: A digital twin
  framework for appetite, feeding decisions and growth in salmonid fish.
  submitted.

This code contains the FishMet basic algorithm and some wrapping code needed to
run it in a single run batch mode. But it does not include the user interfaces
and other non-essential parts that are described in the above Budaev, Cusimanio
& Rønnestad (2024) paper.

FishMet website: https://fishmet.uib.no/

## Support

This project received funding from the European Union’s Horizon 2020 research
and innovation programme under grant agreement No 818036, and Research Council
of Norway No 344608. 

## Copyright

The FishMet model is subject to copyright:

Copyright (C) 2024 Sergey Budaev and Ivar Rønnestad \
Copyright (C) 2024 The University of Bergen, Norway

This program is free software: you can redistribute it and/or modify it under
the terms of the **GNU Affero General Public License** as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but without any
warranty: THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS
AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE
RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR
OR CORRECTION.

See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see https://www.gnu.org/licenses/agpl-3.0.html

This project includes parts licensed under the BSD License, MIT License,
and public domain contributions from NIST. See comments within individual
source files for details.

## Requirements

- Fortran compiler, such as GNU Gfortran
- GNU Make
- Doxygen to build developer documentation
- Asciidoc to build manpage

## Build

Debug build:

    make DEBUG=1

Optimized build

    make

Build documentation

    make docs

Build manpage (Unix style)

    make manpage

(To view the manpage without installing it use this: `man ./FishMet.1`)

Cleanup temporary files

    make clean

or for complete cleanup    

    make distclean

## Run

### Linux

Help screen

    ./fishmet.exe -h

Normal run (batch)

    ./fishmet.exe

### Windows

Help screen

    fishmet.exe -h

Normal run (batch)

    fishmet.exe

