!> @file m_strings.f90
!! Additional module for the FishMet model: Character string manipulation.

! ------------------------------------------------------------------------------
! Copyright notice: Parts of this source code are
!   Copyright (c) 2015-2024 Sergey Budaev
!   Copyright (c) 2008 George Benthien
! ------------------------------------------------------------------------------

!*******************************************************************************
! SVN $Id: m_strings.f90 16565 2024-10-10 00:04:30Z sbu062 $
!*******************************************************************************
! PURPOSE:
!> Fortran String Utilities
!! A collection of string manipulation routines is contained in the module
!! ‘strings’ found in the file stringmod.f90. To obtain this module as well as
!! some other string utilities, go to the website
!!     http://www.gbenthien.net/strings/index.html. (no license)
! To use the routines in the module ‘strings’ the user needs to add the
! statement
!     Use BASE_STRINGS
! to the top of the program. These routines were developed primarily to aid
! in the reading and manipulation of input data from an ASCII text file.
!*******************************************************************************
module BASE_STRINGS

implicit none

! Real kinds
integer, parameter :: kr4=selected_real_kind(6,37)   ! single precision real
integer, parameter :: kr8=selected_real_kind(15,307) ! double precision real

! Integer kinds
integer, parameter :: ki4=selected_int_kind(9)       ! single precision integer
integer, parameter :: ki8=selected_int_kind(18)      ! double precision integer

private :: kr4,kr8,ki4,ki8
private :: VALUE_DR,VALUE_SR,VALUE_DI,VALUE_SI

!> Generic interface for converting a number string to a
!! number. Calling syntax is 'call value(numstring,number,ios)'
!! where 'numstring' is a number string and 'number' is a
!! real number or an integer (single or double precision).
interface VALUE
   module procedure VALUE_DR
   module procedure VALUE_SR
   module procedure VALUE_DI
   module procedure VALUE_SI
end interface

!**********************************************************************

contains

!> Parses the string 'str' into arguments args(1), ..., args(nargs) based on
!! the delimiters contained in the string 'delims'. Preceding a delimiter in
!! 'str' by a backslash (\) makes this particular instance not a delimiter.
!! The integer output variable nargs contains the number of arguments found.
pure subroutine PARSE(str_in,delims,args,nargs)
  character(len=*), intent(in) :: str_in
  character(len=*), intent(in) :: delims
  character(len=*),dimension(:), intent(inout) :: args
  integer(ki4), intent(out) :: nargs

  character(len=:), allocatable :: str
  integer(ki4) :: i, k, lenstr, na

  str=str_in

  call compact(str)
  na=size(args)
  do i=1,na
    args(i)=' '
  end do
  nargs=0
  lenstr=len_trim(str)
  if(lenstr==0) return
  k=0

  do
     if(len_trim(str) == 0) exit
     nargs=nargs+1
     call SPLIT(str,delims,args(nargs))
     call REMOVEBKSL(args(nargs))
  end do

end subroutine PARSE


!> Converts multiple spaces and tabs to single spaces; deletes control
!! characters; removes initial spaces.
pure subroutine COMPACT(str)
  character(len=*), intent(inout):: str

  character(len=1):: ch
  character(len=len_trim(str)):: outstr
  integer(ki4) :: i, k, ich, isp, lenstr

  str=adjustl(str)
  lenstr=len_trim(str)
  outstr=' '
  isp=0
  k=0

  do i=1,lenstr
    ch=str(i:i)
    ich=iachar(ch)

    select case(ich)

      case(9,32)     ! space or tab character
        if(isp==0) then
          k=k+1
          outstr(k:k)=' '
        end if
        isp=1

      case(33:)      ! not a space, quote, or control character
        k=k+1
        outstr(k:k)=ch
        isp=0

    end select

  end do

  str=adjustl(outstr)

end subroutine COMPACT


!> Converts number string to a double precision real number
pure subroutine VALUE_DR(str,rnum,ios)
  character(len=*), intent(in)  :: str
  real(kr8),        intent(out) :: rnum
  integer,          intent(out) :: ios

  integer(ki4) :: ipos, ilen

  ilen=len_trim(str)
  ipos=scan(str,'Ee')
  if(.not.is_digit(str(ilen:ilen)) .and. ipos/=0) then
     ios=3
     return
  end if
  read(str,*,iostat=ios) rnum

end subroutine VALUE_DR


!> Converts number string to a single precision real number
pure subroutine VALUE_SR(str,rnum,ios)
  character(len=*), intent(in)  :: str
  real(kr4),        intent(out) :: rnum
  integer(ki4),     intent(out) :: ios

  real(kr8) :: rnumd

  call value_dr(str,rnumd,ios)
  if( abs(rnumd) > huge(rnum) ) then
    ios=15
    return
  end if
  if( abs(rnumd) < tiny(rnum) ) rnum=0.0_kr4
  rnum=rnumd

end subroutine VALUE_SR


!> Converts number string to a double precision integer value
pure subroutine VALUE_DI(str,inum,ios)
  character(len=*), intent(in)  :: str
  integer(ki8),     intent(out) :: inum
  integer(ki4),     intent(out) :: ios

  real(kr8) :: rnum

  call value_dr(str,rnum,ios)
  if(abs(rnum)>huge(inum)) then
    ios=15
    return
  end if
  inum=nint(rnum,ki8)

end subroutine VALUE_DI


!> Converts number string to a single precision integer value
pure subroutine VALUE_SI(str,inum,ios)
  character(len=*), intent(in)  :: str
  integer(ki4),     intent(out) :: inum
  integer(ki4),     intent(out) :: ios

  real(kr8) :: rnum

  call value_dr(str,rnum,ios)
  if(abs(rnum)>huge(inum)) then
    ios=15
    return
  end if
  inum=nint(rnum,ki4)

end subroutine VALUE_SI


!> Converts string to upper case
pure function UPPERCASE(str) result(ucstr)
  character (len=*), intent(in) :: str
  character (len=len_trim(str)):: ucstr

  integer(ki4) :: i, iqc, iav, iquote, ioffset, ilen

  ilen=len_trim(str)
  ioffset=iachar('A')-iachar('a')
  iquote=0
  ucstr=str
  do i=1,ilen
    iav=iachar(str(i:i))
    if(iquote==0 .and. (iav==34 .or.iav==39)) then
      iquote=1
      iqc=iav
      cycle
    end if
    if(iquote==1 .and. iav==iqc) then
      iquote=0
      cycle
    end if
    if (iquote==1) cycle
    if(iav >= iachar('a') .and. iav <= iachar('z')) then
      ucstr(i:i)=achar(iav+ioffset)
    else
      ucstr(i:i)=str(i:i)
    end if
  end do

end function UPPERCASE


!> Converts string to lower case
pure function LOWERCASE(str) result(lcstr)
  character (len=*), intent(in) :: str
  character (len=len_trim(str)) :: lcstr

  integer(ki4) :: iqc, iav, i, iquote, ioffset, ilen

  ilen=len_trim(str)
  ioffset=iachar('A')-iachar('a')
  iquote=0
  lcstr=str
  do i=1,ilen
    iav=iachar(str(i:i))
    if(iquote==0 .and. (iav==34 .or.iav==39)) then
      iquote=1
      iqc=iav
      cycle
    end if
    if(iquote==1 .and. iav==iqc) then
      iquote=0
      cycle
    end if
    if (iquote==1) cycle
    if(iav >= iachar('A') .and. iav <= iachar('Z')) then
      lcstr(i:i)=achar(iav-ioffset)
    else
      lcstr(i:i)=str(i:i)
    end if
  end do

end function LOWERCASE


! Returns .true. if ch is a digit (0,1,...,9) and .false. otherwise
pure function IS_DIGIT(ch) result(res)
  character, intent(in) :: ch
  logical :: res

  select case(ch)
  case('0':'9')
    res=.true.
  case default
    res=.false.
  end select

end function IS_DIGIT


!> Find the first instance of a character from 'delims' in the
!! the string 'str'. The characters before the found delimiter are
!! output in 'before'. The characters after the found delimiter are
!! output in 'str'. The optional output character 'sep' contains the
!! found delimiter. A delimiter in 'str' is treated like an ordinary
!! character if it is preceded by a backslash (\). If the backslash
!! character is desired in 'str', then precede it with another
!! backslash.
pure subroutine SPLIT(str,delims,before,sep)
  character(len=*),   intent(inout) :: str
  character(len=*),   intent(in)    :: delims
  character(len=*),   intent(out)   :: before
  character,optional, intent(out)   :: sep

  logical :: pres
  character :: ch,cha

  integer(ki4) :: i, k, ipos, iposa, ibsl, lenstr

  pres=present(sep)
  str=adjustl(str)
  call compact(str)
  lenstr=len_trim(str)
  if(lenstr == 0) return        ! string str is empty
  k=0
  ibsl=0                        ! backslash initially inactive
  before=' '
  do i=1,lenstr
     ch=str(i:i)
     if(ibsl == 1) then          ! backslash active
        k=k+1
        before(k:k)=ch
        ibsl=0
        cycle
     end if
     if(ch == '\') then          ! backslash with backslash inactive
        k=k+1
        before(k:k)=ch
        ibsl=1
        cycle
     end if
     ipos=index(delims,ch)
     if(ipos == 0) then          ! character is not a delimiter
        k=k+1
        before(k:k)=ch
        cycle
     end if
     if(ch /= ' ') then          ! character is a delimiter that is not a space
        str=str(i+1:)
        if(pres) sep=ch
        exit
     end if
     cha=str(i+1:i+1)            ! character is a space delimiter
     iposa=index(delims,cha)
     if(iposa > 0) then          ! next character is a delimiter
        str=str(i+2:)
        if(pres) sep=cha
        exit
     else
        str=str(i+1:)
        if(pres) sep=ch
        exit
     end if
  end do
  if(i >= lenstr) str=''
  str=adjustl(str)              ! remove initial spaces

end subroutine SPLIT


!> Removes backslash (`\`) characters. Double backslashes (`\\`)
!! are replaced by a single backslash.
pure subroutine REMOVEBKSL(str)
  character(len=*), intent(inout):: str

  character(len=1):: ch
  character(len=len_trim(str))::outstr
  integer(ki4) :: i, k, ibsl, lenstr

  str=adjustl(str)
  lenstr=len_trim(str)
  outstr=' '
  k=0
  ibsl=0                        ! backslash initially inactive

  do i=1,lenstr
    ch=str(i:i)
    if(ibsl == 1) then          ! backslash active
     k=k+1
     outstr(k:k)=ch
     ibsl=0
     cycle
    end if
    if(ch == '\') then          ! backslash with backslash inactive
     ibsl=1
     cycle
    end if
    k=k+1
    outstr(k:k)=ch              ! non-backslash with backslash inactive
  end do

  str=adjustl(outstr)

end subroutine REMOVEBKSL

!**********************************************************************

!> IS_NUMERIC
!! PURPOSE: Checks if a string is a string representation of a number, i.e.
!!          consists only of digits and ".," characters.
!! CALL PARAMETERS:
!!    Character string
!!    Logical flag to include some non-numeric space characters: space and
!!     horizontal tab.
!! Author: Sergey Budaev
pure function IS_NUMERIC(string, include_blank) result (num_flag)

  character(len=*), intent(in)  :: string
  logical, optional, intent(in) :: include_blank
  logical :: num_flag

  ! Local
  integer :: i
  logical :: include_blank_here
  character, parameter :: TAB =achar(9)
  character, parameter :: DQT =achar(34) ! double quote "
  character, parameter :: SQT =achar(39) ! single quote '

  num_flag = .TRUE.

  if (len_trim(string)==0) then
    num_flag=.FALSE.
    return
  end if

  if(present(include_blank)) then
    include_blank_here = include_blank
  else
    include_blank_here = .FALSE.
  end if

  if (include_blank_here) then
    do i = 1, len_trim(string)
      if ( .not. is_digit(string(i:i)) .and.                                  &
            string(i:i)/="-" .and.                                            &
            string(i:i)/="E" .and.                                            &
            string(i:i)/="." .and.                                            &
            string(i:i)/="," .and.                                            &
            string(i:i)/=" " .and.                                            &
            string(i:i)/=DQT .and.                                            &
            string(i:i)/=SQT .and.                                            &
            string(i:i)/=TAB ) num_flag = .FALSE.
    end do
  else
    do i = 1, len_trim(string)
      if ( .not. is_digit(string(i:i)) .and.                                  &
            string(i:i)/="-" .and.                                            &
            string(i:i)/="E" .and.                                            &
            string(i:i)/="." .and.                                            &
            string(i:i)/="," ) num_flag = .FALSE.
    end do
  end if

end function IS_NUMERIC


!> OUTSTRIP
!! PURPOSE: removes (strips) a set of characters from a string
!!
!! CALL PARAMETERS:
!!    Character string to be stripped
!!    Character string containing the set of characters to be stripped
!!
!! Author: Rosetta code
!! https://www.rosettacode.org/wiki/Strip_a_set_of_characters_from_a_string#Fortran
elemental subroutine OUTSTRIP(string,set)
  character(len=*), intent(inout) :: string
  character(len=*), intent(in)    :: set
  integer                         :: old, new, stride

  old = 1; new = 1
  do
    stride = scan( string( old : ), set )
    if ( stride > 0 ) then
      string( new : new+stride-2 ) = string( old : old+stride-2 )
      old = old+stride
      new = new+stride-1
    else
      string( new : ) = string( old : )
      return
    end if
  end do
end subroutine OUTSTRIP

end module BASE_STRINGS
