!> @file m_simulate.f90
!! The FishMet Model: Runtime behaviour of the model.

! ------------------------------------------------------------------------------
! Copyright notice:
!   Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad
! FishMet model code is distributed according to GNU AGPL v3, see LICENSE.md
! ------------------------------------------------------------------------------

! Notes:
!         This module is for running the simulation. It is called from the
!         user interface (CMD or GUI) modules.

!> Module that defines the runtime behaviour of the model
module SIMULATE

  use REALCALC
  use COMMONDATA
  use BASE_UTILS, only : TOSTR
  use BASE_STRINGS, only : LOWERCASE
  use THE_FISH

  implicit none

  !> Flag for extended/detailed debug logging
  logical, parameter :: IS_EXTENDED_LOGGING_DEBUG_TEST = .FALSE.

  !> Flag that defines how urinal and branchial energy loss (UE+ZE) is
  !! calculated for model output arrays. If TRUE, UE+ZE is computed in
  !! kJ per kg per day (default), otherwise, in O2 equivalent unit:
  !! mg O2 / kg/hour
  logical, parameter, private :: IS_URINAL_BRANCHIAL_KJ_DAY = .TRUE.

  !> Progress bar widget ID for the GUI mode, needed to update at runtime
  integer, public :: gui_idx_progress

  ! Model output arrays that are used to build the output plots and data
  ! exports
  !> This data structure keeps the output values for each time step. It should
  !! normally be instantiated as an allocatable array of the size equal to the
  !! total number of time steps in the model run.
  type, public :: OUTPUT_ARRAYS_STEP_DEF
    !> Total cumulative number of food items encountered by (provided to)
    !! the fish.
    integer :: food_items_encountered
    !> Total cumulative number of food items ingested.
    integer :: food_items_ingested_total
    !> Total cumulative number of food items **not** ingested.
    integer :: food_items_not_ingested_total
    !> Number of food items in stomach
    integer :: n_food_items_stomach
    !> Number of food items in midgut
    integer :: n_food_items_midgut
    !> Overall appetite value as computed by the_fish::mid_gut::appetite().
    real(SRP) :: fish_appetite
    !> Total mass of food in the **stomach**.
    real(SRP) :: food_mass_total_stomach
    !> Total mass of food in the **midgut**.
    real(SRP) :: food_mass_total_midgut
    !> Total mass of food absorped in the mid-gut
    !! Note: not saved in output file, debug only.
    real(SRP) :: food_mass_absorb_total_midgut
    !> Cumulative mass absorped in the midgut.
    !! Note: not saved in output file, debug only.
    real(SRP) :: food_mass_absorb_cumulate
    !> Instantaneour absorption rate in the midgut.
    real(SRP) :: food_mass_absorb_rate_instant
    !> Total oxygen uptake
    real(SRP) :: oxygen_uptake_total
    !> Baseline standard metabilic rate (SMR)
    real(SRP) :: baseline_smr
    !> Active metabolic rate, excluding SMR
    real(SRP) :: active_amr
    !> Branchaial and urinal metabolic rate
    real(SRP) :: branchial_ue_ze
    !> Specific dynamic action
    real(SRP) :: digestion_cost_sda
    !> Cumulative mass of evacuated food remains, stepwise
    real(SRP) :: mass_evacuated_cumulative
    !> Energy balance of the fish:
    !! the_fish::history_array_fish::energy_balance_curr
    real(SRP) :: energy_balance_total
    !> Body mass of the fish
    !! the_fish::history_array_fish::body_mass_current
    real(SRP) :: body_mass_dynamics
    !> Activity of the fish calculated as
    !! `fish%activity_baseline() * fish%activity_appetite()`
    !! TODO: current activity is only used in fish_amr_locomotion_energy_cost()
    !!       so may need a separate `fish%activity()` function.
    real(SRP) :: current_activity

  end type OUTPUT_ARRAYS_STEP_DEF

  !> ::output_arrays instantiates the global data structure that
  !! keeps the model output arrays.
  type(OUTPUT_ARRAYS_STEP_DEF), dimension(:), allocatable, public ::          &
                                                                Output_Arrays

  !> Overall output statistics that are output from the model.
  !! @note Note that thesediffer from ::output_arrays_step_def in that
  !!       they are not arrays.
  type, public :: OUTPUT_STATS_TOTAL_DEF
    real(SRP) :: total_mass_food_ingested
    real(SRP) :: total_mass_evacuated
  end type

  !> ::output_stats instantiates non-array global data structure that keeps
  !! total model output statistics, that are not arrays by time step.
  type(OUTPUT_STATS_TOTAL_DEF) :: Output_Stats

  contains

  !> Calculate the total number of time steps in the model from the number
  !! of hours. This in fact converts hours to seconds.
  pure function total_timesteps(hours) result (time_steps)
    !> Optional hours to be converted to seconds. If absent, the
    !! default value is obtained from the global parameter
    !! commondata::global_run_model_hours.
    integer, optional, intent(in) :: hours
    integer(LONG) :: time_steps

    if (present(hours)) then
      time_steps = hours * HOUR
    else
      time_steps = Global_Run_Model_Hours * HOUR
    end if

  end function total_timesteps

  !> Allocate *model output arrays*  ::output_arrays
  impure subroutine output_arrays_init( time_steps )
    !> Optional overall number of time steps of the model
    !! @note Note that this argument has the integer type of the kind
    !!       commondata::long. This might taint default integer values.
    !! @note This procedure cannot be `pure` because it allocates a
    !!       global module-scope array.
    integer(LONG), optional, intent(in) :: time_steps

    integer(LONG) :: time_steps_loc

    if (present(time_steps)) then
      time_steps_loc = time_steps
    else
      time_steps_loc = total_timesteps()
    end if

    if (.not. allocated(Output_Arrays)) then
      allocate(Output_Arrays(time_steps_loc))
    else
      deallocate(Output_Arrays)
      allocate(Output_Arrays(time_steps_loc))
    end if

    Output_Arrays%food_items_encountered = 0
    Output_Arrays%food_items_ingested_total = 0
    Output_Arrays%food_items_not_ingested_total = 0
    Output_Arrays%n_food_items_stomach = 0
    Output_Arrays%n_food_items_midgut = 0
    Output_Arrays%fish_appetite = MISSING
    Output_Arrays%food_mass_total_stomach = 0.0_SRP
    Output_Arrays%food_mass_total_midgut = 0.0_SRP
    Output_Arrays%food_mass_absorb_total_midgut = 0.0_SRP
    Output_Arrays%food_mass_absorb_cumulate = 0.0_SRP
    Output_Arrays%food_mass_absorb_rate_instant = 0.0_SRP
    Output_Arrays%oxygen_uptake_total = 0.0_SRP
    Output_Arrays%baseline_smr = 0.0_SRP
    Output_Arrays%active_amr = 0.0_SRP
    Output_Arrays%branchial_ue_ze = 0.0_SRP
    Output_Arrays%digestion_cost_sda = 0.0_SRP
    Output_Arrays%mass_evacuated_cumulative = 0.0_SRP
    Output_Arrays%energy_balance_total = 0.0_SRP
    Output_Arrays%body_mass_dynamics = 0.0_SRP
    Output_Arrays%current_activity = 0.0_SRP

    Output_Stats%total_mass_food_ingested = 0.0_SRP
    Output_Stats%total_mass_evacuated = 0.0_SRP

  end subroutine output_arrays_init

  !> Update model *output arrays* ::output_arrays.
  impure subroutine output_arrays_update_step( this_fish, time_step, temperature )
    !> The fish that is participating in the simulation run.
    class(FISH), intent(in) :: this_fish
    !> Optional time step, which coincides with the index of the model
    !! output arrays. The default value used if the argument is not given
    !! is defined by the global time step commondata::global_time_step.
    integer(LONG), optional, intent(in) :: time_step
    !> Optional temperature
    real(SRP), optional, intent(in) :: temperature

    ! Local copies of optionals
    integer(LONG) :: time_step_loc
    real(SRP) :: temp_loc

    if (present(time_step)) then
      time_step_loc = time_step
    else
      time_step_loc = Global_Time_Step
    end if

    if (present(temperature)) then
      temp_loc = temperature
    else
      temp_loc = Global_Temperature
    end if

    associate ( aout => Output_Arrays(time_step_loc),                         &
                foods_midgut =>                                               &
                  this_fish%food_items_midgut(1:this_fish%n_food_items_midgut) )

      aout%fish_appetite = this_fish%appetite()
      aout%food_mass_total_stomach = this_fish%st_food_mass()
      aout%food_mass_total_midgut = this_fish%mg_food_mass()
      aout%n_food_items_stomach = this_fish%n_food_items_stomach
      aout%n_food_items_midgut = this_fish%n_food_items_midgut -              &
                                                this_fish%n_food_items_stomach
      aout%food_mass_absorb_total_midgut =                                    &
        sum( foods_midgut%absorped, foods_midgut%absorped /= MISSING )

      ! Update cumulative absorption in the output arrays
      aout%food_mass_absorb_cumulate =                                        &
                          this_fish%history%food_mass_absorb_cum(HISTORY_SIZE)

      ! Update instantaneous absorption rate.
      aout%food_mass_absorb_rate_instant = this_fish%absorption_rate()

      !> Update oxygen uptake, convert to mg O2 per kg of body mass per hour
      aout%oxygen_uptake_total =                                              &
                      HOUR * g2mg(this_fish%uptake_o2(temperature=temp_loc,   &
                                                      is_mass=.TRUE.)) /      &
                                                      g2kg(this_fish%mass())

      !> Update baseline meabolic rate, mg O2 per kg per hour
      aout%baseline_smr =                                                     &
                    this_fish%smr(temperature=temp_loc, is_per_hour=.TRUE.) / &
                    g2kg(this_fish%mass())

      !> Update Active metabolic rate, mg O2 per kg per hour
      aout%active_amr =                                                       &
                    this_fish%amr(temperature=temp_loc,                       &
                                  is_per_hour=.TRUE.,is_exclude_smr=.FALSE.) /&
                                  g2kg( this_fish%mass() )

      !> Update SDA, mg O2 per hour per kg
      aout%digestion_cost_sda =                                               &
                     ( this_fish%smr(temperature=temp_loc, is_per_hour=.TRUE.)&
                       * this_fish%sda_fact( Global_SDA_Absorp_Rate_Max,      &
                                             Global_SDA_Factor_Max) )         &
                       / g2kg(this_fish%mass())

      if (IS_URINAL_BRANCHIAL_KJ_DAY) then
        !> Update urinal and branchial energy in kJ/kg per day
        aout%branchial_ue_ze = 24 * HOUR *                                    &
                  this_fish%ue_ze(temperature=temp_loc, is_oxygen=.FALSE.)    &
                  / g2kg(this_fish%mass())
      else
        !> Alternatively, urinal and branchial energy is output as equivalent
        !! mg O2 per kg per hour
        !! @warning This makes sense only if UE+ZE is defined as factor of SMR
        !!          rather than fixed rate (see
        !!          commondata::global_is_ue_ze_fixed_rate)
        aout%branchial_ue_ze = HOUR *                                         &
                  g2mg(this_fish%ue_ze(temperature=temp_loc,is_oxygen=.TRUE.))&
                  / g2kg(this_fish%mass())
      end if

      ! Update cumulative evacuation, note that `this_fish%total_mass_evacuated`
      ! keeps cumulative total, so cumulative is obtained straightforward.
      aout%mass_evacuated_cumulative =                                        &
                                last(this_fish%history%total_mass_evacuated)

      ! Update the energy balance from the last value of the respective
      ! history array
      aout%energy_balance_total = last(this_fish%history%energy_balance_curr)

      ! Update the body mass dynamics over the all simulation history
      aout%body_mass_dynamics = last(this_fish%history%body_mass_current)
      aout%current_activity =                                                 &
                this_fish%activity_baseline() * this_fish%activity_appetite()

      ! Update the output stats, non-arrays
      Output_Stats%total_mass_food_ingested =                                 &
                              last(this_fish%history%total_mass_food_ingested)
      Output_Stats%total_mass_evacuated =                                     &
                              last(this_fish%history%total_mass_evacuated)

    end associate

  end subroutine output_arrays_update_step

  !> Update the cumulative count of food items ingested. This cannot normally
  !! be done in ::output_arrays_update_step() and requires an input argument.
  impure subroutine output_arrays_add_ingested(                               &
                                    is_given, is_eaten, time_step, add_eaten )
    !> Logical indicator whether a fod item is ingested at the current time
    !! step (TRUE) or not (FALSE). The cumulative number of food items
    !! consumed is updated (added) only if this parameter is TRUE.
    logical, intent(in) :: is_given
    !> Logical indicator whether a fod item is ingested at the current time
    !! step (TRUE) or not (FALSE). The cumulative number of food items
    !! consumed is updated (added) only if this parameter is TRUE.
    logical, intent(in) :: is_eaten
    !> Optional time step, which coincides with the index of the model
    !! output arrays. The default value used if the argument is not given
    !! is defined by the global time step commondata::global_time_step.
    integer(LONG), optional, intent(in) :: time_step
    !> Optional number of food items ingested, default value is 1.
    integer, optional, intent(in) :: add_eaten

    ! Local copies of optionals
    integer(LONG) :: time_step_loc
    integer :: add_eaten_loc

    if (present(time_step)) then
      time_step_loc = time_step
    else
      time_step_loc = Global_Time_Step
    end if

    if (present(add_eaten)) then
      add_eaten_loc = add_eaten
    else
      add_eaten_loc = 1
    end if

    if ( time_step_loc == 1 ) then
      if ( is_eaten ) then
        Output_Arrays(time_step_loc)%food_items_ingested_total = 1
      else
        Output_Arrays(time_step_loc)%food_items_ingested_total = 0
      end if
      if ( is_given .and. .not. is_eaten ) then
        Output_Arrays(time_step_loc)%food_items_not_ingested_total = 1
      else
        Output_Arrays(time_step_loc)%food_items_not_ingested_total = 0
      end if
      Output_Arrays(time_step_loc)%food_items_encountered =                   &
                Output_Arrays(time_step_loc)%food_items_ingested_total +      &
                Output_Arrays(time_step_loc)%food_items_not_ingested_total
      return
    end if

    !> ### Implementation notes ###
    !> To update the cumulative count of food items ingested, the number of
    !! items ingested at this time step is added to the value at the previous
    !! time step. At the first step the counter is set to zero.
    if ( is_eaten ) then
      Output_Arrays(time_step_loc)%food_items_ingested_total =                &
        Output_Arrays(time_step_loc-1)%food_items_ingested_total + add_eaten_loc
    else
      Output_Arrays(time_step_loc)%food_items_ingested_total =                &
                    Output_Arrays(time_step_loc-1)%food_items_ingested_total
    end if

    !> To update the cumulative count of food items not ingested, the number of
    !! items not ingested at this time step is added to the value at the
    !! previous time step. At the first step the counter is set to zero.
    if ( is_given .and. .not. is_eaten ) then
      Output_Arrays(time_step_loc)%food_items_not_ingested_total =            &
        Output_Arrays(time_step_loc-1)%food_items_not_ingested_total          &
                                                                + add_eaten_loc
    else
      Output_Arrays(time_step_loc)%food_items_not_ingested_total =            &
                    Output_Arrays(time_step_loc-1)%food_items_not_ingested_total
    end if

    Output_Arrays(time_step_loc)%food_items_encountered =                     &
          Output_Arrays(time_step_loc)%food_items_ingested_total +            &
          Output_Arrays(time_step_loc)%food_items_not_ingested_total

  end subroutine output_arrays_add_ingested

  !> Save the model output arrays to a CSV data file.
  impure subroutine output_arrays_save_csv( file_name, is_gui_dialog,         &
                                                              is_write_error )

    use CSV_IO
    !> The name of the CSV file to save the data into.
    character(len=*), intent(in) :: file_name
        !> Optional indicator that error should report to GUI.
    logical, optional, intent(in) :: is_gui_dialog
    !> Optional file error indicator flag, if TRUE, reports error
    logical, optional, intent(out) :: is_write_error

    logical :: is_gui_dialog_loc

    !> - `handle_csv` is the CSV file handle object defining the file name,
    !!   Fortran unit and error descriptor, see HEDTOOLS manual for details.
    type(CSV_FILE) :: handle_csv
    !> - `csv_record` is the temporary character string that keeps the
    !!   whole record of the file, i.e. the whole row of the spreadsheet table.
    character(len=:), allocatable :: csv_record
    !> - `COLUMNS` is a parameter array that keeps all column headers; its
    !!   size is equal to the total number of variables (columns) in the data
    !!   spreadsheet file.
    !! .
    character(len=LABEL_LEN), dimension(*), parameter ::                      &
        COLUMNS =  [ character(len=LABEL_LEN) ::                              &
                                                    "DAY_NIGHT     ",   &   ! 1
                                                    "SCHEDULE_FOOD ",   &   ! 2
                                                    "FOOD_PROVIDED ",   &   ! 3
                                                    "INGESTED_SUM  ",   &   ! 4
                                                    "NOT_INGESTED  ",   &   ! 5
                                                    "N_FOOD_STOM   ",   &   ! 6
                                                    "N_FOOD_MIDGUT ",   &   ! 7
                                                    "APPETITE      ",   &   ! 8
                                                    "STOMACH_MASS  ",   &   ! 9
                                                    "MIDGUT_MASS   ",   &   !10
                                                    "ABSORP_CUMUL  ",   &   !11
                                                    "ABSORP_INRATE ",   &   !12
                                                    "OXYGEN_UPTAKE ",   &   !13
                                                    "SMR           ",   &   !14
                                                    "AMR           ",   &   !15
                                                    "SDA           ",   &   !16
                                                    "UE_ZE         ",   &   !17
                                                    "ENERGY_BALANCE",   &   !18
                                                    "BODY_MASS     ",   &   !19
                                                    "ACTIVITY      ",   &   !20
                                                    "EVACUATION    " ]      !21

    integer(LONG) :: step

    if (present(is_gui_dialog)) then
      is_gui_dialog_loc = is_gui_dialog
    else
      is_gui_dialog_loc = .FALSE.
    end if

    handle_csv%name = file_name

    call CSV_OPEN_WRITE( handle_csv )

    if ( .not. handle_csv%status ) then
      call csv_file_error_report( handle_csv, is_gui_dialog=is_gui_dialog_loc )
      if(present(is_write_error)) is_write_error=.TRUE.
      return
    end if

    ! Prepare the character string variable `csv_record` that keeps the
    ! whole record (row) of data in the output CSV data file. The length of
    ! this string should be enough to fit all the record data, otherwise
    ! the record is truncated.
    csv_record = repeat( " ", size(COLUMNS) * len(COLUMNS(1)) )

    ! Create and write column header data
    call CSV_RECORD_APPEND( csv_record, COLUMNS )
    call CSV_RECORD_WRITE ( csv_record, handle_csv )

    ! The actual data are written to the CSV file in a loop over all the
    ! time steps of Output_Arrays size
    RECORD_WRT: do step = 1, size(Output_Arrays)

      ! the `csv_record` character string variable is produced such
      ! that it can fit the whole record;
      csv_record = repeat(" ",                                            &
          max( CSV_GUESS_RECORD_LENGTH(size(COLUMNS)+2, 0.0_SRP), LABEL_LEN ) )

      ! Build the record: append variables (columns) one by one
      associate ( dat => Output_Arrays(step) )
        if ( is_day(step) ) then
          call CSV_RECORD_APPEND(csv_record, "DAY")                             ! 1
        else
          call CSV_RECORD_APPEND(csv_record, "NIGHT")
        end if
        call CSV_RECORD_APPEND(csv_record,                                    &
                                     TOSTR(Global_Interval_Food_Pattern(step))) ! 2
        call CSV_RECORD_APPEND(csv_record, dat%food_items_encountered)          ! 3
        call CSV_RECORD_APPEND(csv_record, dat%food_items_ingested_total)       ! 4
        call CSV_RECORD_APPEND(csv_record, dat%food_items_not_ingested_total)   ! 5
        call CSV_RECORD_APPEND(csv_record, dat%n_food_items_stomach)            ! 6
        call CSV_RECORD_APPEND(csv_record, dat%n_food_items_midgut)             ! 7
        call CSV_RECORD_APPEND(csv_record, dat%fish_appetite)                   ! 8
        call CSV_RECORD_APPEND(csv_record, dat%food_mass_total_stomach)         ! 9
        call CSV_RECORD_APPEND(csv_record, dat%food_mass_total_midgut)          !10
        call CSV_RECORD_APPEND(csv_record, dat%food_mass_absorb_cumulate )      !11
        call CSV_RECORD_APPEND(csv_record, dat%food_mass_absorb_rate_instant)   !12
        call CSV_RECORD_APPEND(csv_record, dat%oxygen_uptake_total)             !13
        call CSV_RECORD_APPEND(csv_record, dat%baseline_smr)                    !14
        call CSV_RECORD_APPEND(csv_record, dat%active_amr)                      !15
        call CSV_RECORD_APPEND(csv_record, dat%digestion_cost_sda)              !16
        call CSV_RECORD_APPEND(csv_record, dat%branchial_ue_ze)                 !17
        call CSV_RECORD_APPEND(csv_record, dat%energy_balance_total)            !18
        call CSV_RECORD_APPEND(csv_record, dat%body_mass_dynamics)              !19
        call CSV_RECORD_APPEND(csv_record, dat%current_activity)                !20
        call CSV_RECORD_APPEND(csv_record, dat%mass_evacuated_cumulative)       !21
      end associate

      ! Write the step-s data record
      call CSV_RECORD_WRITE( csv_record, handle_csv )
      if ( .not. handle_csv%status ) then
        call csv_file_error_report(handle_csv, is_gui_dialog=is_gui_dialog_loc)
        call CSV_CLOSE( handle_csv )
        if(present(is_write_error)) is_write_error=.TRUE.
        return
      end if

    end do RECORD_WRT

    call CSV_CLOSE( handle_csv )

    if(present(is_write_error)) is_write_error=.FALSE.

  end subroutine output_arrays_save_csv

  !> Save the rate data arrays to a CSV data file.
  impure subroutine output_arrays_save_rate_data_csv ( file_name,            &
                                                       is_gui_dialog,        &
                                                       is_write_error )
    use CSV_IO
    !> The name of the CSV file to save the data into.
    character(len=*), intent(in) :: file_name
    !> Optional indicator that error should report to GUI.
    logical, optional, intent(in) :: is_gui_dialog
    !> Optional file error indicator flag, if TRUE, reports error
    logical, optional, intent(out) :: is_write_error

    logical :: is_gui_dialog_loc

    integer :: i, rate_unit_loc, raw_scale_max, rate_scale_y
    real(SRP), dimension(:), allocatable :: rows_x,                           &
                                            y_rate_ingested,                  &
                                            y_absorption_rate,                &
                                            y_growth_rate,                    &
                                            y_specific_growth_rate

    !> - `handle_csv` is the CSV file handle object defining the file name,
    !!   Fortran unit and error descriptor, see HEDTOOLS manual for details.
    type(CSV_FILE) :: handle_csv
    !> - `csv_record` is the temporary character string that keeps the
    !!   whole record of the file, i.e. the whole row of the spreadsheet table.
    character(len=:), allocatable :: csv_record
    character(len=LABEL_LEN), dimension(*), parameter ::                      &
        COLUMNS =  [ character(len=LABEL_LEN) ::    "INTERVAL_HOUR",   & ! 1
                                                    "INGEST_RATE",     & ! 2
                                                    "ABSORP_RATE",     & ! 3
                                                    "GROWTH_RATE",     & ! 4
                                                    "SP_GROWTH_RATE"   ] ! 5

    if (present(is_gui_dialog)) then
      is_gui_dialog_loc = is_gui_dialog
    else
      is_gui_dialog_loc = .FALSE.
    end if

    handle_csv%name = file_name

    call CSV_OPEN_WRITE( handle_csv )

    if ( .not. handle_csv%status ) then
      call csv_file_error_report( handle_csv, is_gui_dialog=is_gui_dialog_loc )
      if(present(is_write_error)) is_write_error=.TRUE.
      return
    end if

    ! Prepare the character string variable `csv_record` that keeps the
    ! whole record (row) of data in the output CSV data file. The length of
    ! this string should be enough to fit all the record data, otherwise
    ! the record is truncated.
    csv_record = repeat( " ", size(COLUMNS) * len(COLUMNS(1)) )

    ! Create and write column header data
    call CSV_RECORD_APPEND( csv_record, COLUMNS )
    call CSV_RECORD_WRITE ( csv_record, handle_csv )

    ! Calculate the rate data using the rate interval parameter
    ! commondata::global_rate_interval.
    rate_unit_loc = MINUTE * Global_Rate_Interval
    raw_scale_max = size(Output_Arrays)

    allocate( rows_x(raw_scale_max/rate_unit_loc),                            &
              y_rate_ingested(raw_scale_max/rate_unit_loc),                   &
              y_absorption_rate(raw_scale_max/rate_unit_loc),                 &
              y_growth_rate(raw_scale_max/rate_unit_loc),                     &
              y_specific_growth_rate(raw_scale_max/rate_unit_loc)      )

    rows_x = real([( 1 + (i-1), i=1,raw_scale_max/rate_unit_loc )], SRP)
    y_rate_ingested = cum2rate(Output_Arrays%food_items_ingested_total,       &
                                                                rate_unit_loc)
    y_absorption_rate = cum2rate(Output_Arrays%food_mass_absorb_cumulate,     &
                                                                rate_unit_loc)
    y_growth_rate = blockrate(Output_Arrays%body_mass_dynamics, rate_unit_loc)
    y_growth_rate(1) = 0.0_SRP ! quick fix for the first time interval

    y_specific_growth_rate =                                                  &
              SGR(Output_Arrays%body_mass_dynamics,rate_unit_loc,rate_scale_y)
    y_specific_growth_rate(1) = 0.0_SRP

    ! Write the rate data to the output CSV file
    RECORD_WRT: do i=1, raw_scale_max / rate_unit_loc

      ! the `csv_record` character string variable is produced such
      ! that it can fit the whole record;
      csv_record = repeat(" ",                                                &
          max( CSV_GUESS_RECORD_LENGTH(size(COLUMNS)+2, 0.0_SRP), LABEL_LEN ) )
      call CSV_RECORD_APPEND( csv_record, rows_x(i)*rate_unit_loc/HOUR  )   ! 1
      call CSV_RECORD_APPEND( csv_record, y_rate_ingested(i)*rate_scale_y ) ! 2
      call CSV_RECORD_APPEND( csv_record, y_absorption_rate(i)*rate_scale_y)! 3
      call CSV_RECORD_APPEND( csv_record, y_growth_rate(i)*rate_scale_y )   ! 4
      call CSV_RECORD_APPEND( csv_record, y_specific_growth_rate(i) )       ! 5

      ! Note: Left adjustment of the record with adjustl() is needed because
      ! CSV_RECORD_APPEND() here produces ugly strings when mixing reals and
      ! integers with many zeroes, the string starts with blanks. As a result,
      ! office programs may have difficulty determining the columns in the
      ! resulting CSV file.
      csv_record = adjustl(csv_record)

      ! Write the step-s data record
      call CSV_RECORD_WRITE( csv_record, handle_csv )
      if ( .not. handle_csv%status ) then
        call csv_file_error_report(handle_csv, is_gui_dialog=is_gui_dialog_loc)
        call CSV_CLOSE( handle_csv )
        if(present(is_write_error)) is_write_error=.TRUE.
        return
      end if

    end do RECORD_WRT

    call CSV_CLOSE( handle_csv )

    if(present(is_write_error)) is_write_error=.FALSE.

  end subroutine output_arrays_save_rate_data_csv

  !> Report CSV file write error.
  impure subroutine csv_file_error_report( csv_handle, is_gui_dialog )
    use CSV_IO, only : csv_file
    use, intrinsic :: ISO_FORTRAN_ENV, only : ERROR_UNIT
    type(csv_file), intent(inout) :: csv_handle
    !> Optional indicator that error should report to GUI.
    logical, optional, intent(in) :: is_gui_dialog

    write(ERROR_UNIT,"(a)") "ERROR: cannot open CSV file '" //                &
                              trim(csv_handle%name) // "' for writing."
  end subroutine csv_file_error_report

  !> Save the stomach transport data pattern for a single food item.
  impure subroutine stomach_transport_save_csv(csv_file)
    use CSV_IO, only : CSV_MATRIX_WRITE
    !> The name of the CSV file is given as the argument.
    character(len=*), intent(in) :: csv_file

    integer :: i
    integer, parameter, dimension(*) :: plot_t = [(i,i=0,38000,10)]
    real(SRP), dimension(size(plot_t)) :: data_y

    data_y = st_food_item_mass(plot_t)

    call CSV_MATRIX_WRITE ( reshape(                                          &
                                      [real(plot_t, SRP),                     &
                                       data_y],                               &
                                      [size(plot_t), 2]),                     &
                                      csv_file,                               &
                                      ["TIME       ","FEED_VOLUME"]           &
                                   )

  end subroutine stomach_transport_save_csv

  !> Save the appetite function the_fish::appetite_func() pattern for plotting,
  !! testing or analysis.
  impure subroutine appetite_function_save_csv(csv_file)
    use CSV_IO, only : CSV_MATRIX_WRITE
    !> The name of the CSV file is given as the argument.
    character(len=*), intent(in) :: csv_file

    integer :: i

    ! X grid array from 0.0 to 1.0 with increments INCR
    real(SRP), parameter :: INCR = 0.01
    real(SRP), parameter, dimension(*) :: plot_x =                            &
                                [( INCR * (i-1), i=1, floor((1.0)/INCR + 1) )]
    real(SRP), dimension(size(plot_x)) :: data_y

    data_y = appetite_func( plot_x, Global_Appetite_Logist_A,                 &
                                    Global_Appetite_Logist_R )

    call CSV_MATRIX_WRITE ( reshape(                                          &
                                      [plot_x,                                &
                                       data_y],                               &
                                      [size(plot_x), 2]),                     &
                                      csv_file,                               &
                                      ["RELATIVE_VOL","APPETITE_FAC"]         &
                                   )

  end subroutine appetite_function_save_csv

  !> Produce a text string containing general model output statistics.
  !! @note Note that this procedure outputs table formatted data string for
  !!       terminal output and visual inspection.
  pure function model_output_stats_txt () result( txt_out )
    character(len=:), allocatable :: txt_out

    ! Fortran format speifier for `real` type output values
    character(len=*), parameter :: RFMT = "(f10.4)", IFMT = "(i10)"

    real(SRP) :: ratio_ingested, ratio_fcr

    associate ( OutData => Output_Arrays(size(Output_Arrays)) )

      ratio_ingested = real(OutData%food_items_ingested_total,SRP) /          &
                       real(OutData%food_items_encountered,SRP)

      ! calculate FCR for output F/(M-M0)
      ratio_fcr = FCR(OutData%food_items_ingested_total*Global_Food_Item_Mass,&
                      OutData%body_mass_dynamics -                            &
                                            Output_Arrays(1)%body_mass_dynamics)

      txt_out = CRLF //                                                       &
        "General model output statistics ("                                   &
                            // SVN_VERSION_GLOBAL // ")"// CRLF // CRLF //    &
        " - Simulation duration, hours        : " //                          &
            TOSTR(Global_Run_Model_Hours,IFMT,.FALSE.) // CRLF //             &
        " - Total N of food items ingested    : " //                          &
            TOSTR(OutData%food_items_ingested_total,IFMT,.FALSE.) // CRLF //  &
        " - The mass of food ingested (run), g: " //                          &
            TOSTR( OutData%food_items_ingested_total*                         &
                                   Global_Food_Item_Mass,RFMT,.FALSE.)//CRLF//&
        " - Total mass of food ingested, g    : " //                          &
            TOSTR( Output_Stats%total_mass_food_ingested,RFMT,.FALSE.)//CRLF//&
        " - Total cumulative absorption,g     : " //                          &
            TOSTR(OutData%food_mass_absorb_cumulate,RFMT,.FALSE.) //CRLF//    &
        " - Total energy intake from food, kJ : " //                          &
            !         kJ                   g
            TOSTR(feed_energy(OutData%food_mass_absorb_cumulate ) ,RFMT,      &
                                                        .FALSE.) //CRLF//     &
        " - Total maintenance (at SMR), mg O2*: " //                          &
            TOSTR (smr(Global_Body_Mass, Global_Temperature, .TRUE. )         &
                    * Global_Run_Model_Hours, RFMT,.FALSE. )//CRLF//          &
        " - Total maintenance energy, kJ*     : " //                          &
            !       kJ          l    g     mgO2
            TOSTR ( energy_o2(o2vol(mg2g(smr(Global_Body_Mass,                &
                    Global_Temperature, .TRUE. ) ) )  )                       &
                    * Global_Run_Model_Hours, RFMT,.FALSE. )//CRLF//          &
        " - Energy budget at the end, kJ      : " //                          &
            TOSTR(OutData%energy_balance_total,RFMT,.FALSE.) //CRLF//         &

        " - Body mass at the start, g         : " //                          &
            TOSTR(Output_Arrays(1)%body_mass_dynamics ,RFMT,.FALSE.) //CRLF// &
        " - Body mass at the end, g           : " //                          &
            TOSTR(OutData%body_mass_dynamics,RFMT,.FALSE.) //CRLF//           &
        " - Feed conversion ratio             : " //                          &
            TOSTR(ratio_fcr,RFMT,.FALSE.) //CRLF//                            &
        " - Specific growth rate (overall),%/h: " //                          &
            TOSTR( SGR( OutData%body_mass_dynamics,                           &
                        Output_Arrays(1)%body_mass_dynamics,                  &
                        ! Note that time is given in hours, so rate scale is -1
                        Global_Run_Model_Hours, 0,-1),RFMT,.FALSE.  )//CRLF// &

        ! DEBUG, body mass equivalent only for zero start energy
        " - Body mass equivalent of energy, g*: " //                          &
            TOSTR( energy2mass(OutData%energy_balance_total),RFMT,.FALSE.)//CRLF//&

        " - Total mass evacuated, g           : " //                          &
              TOSTR( Output_Stats%total_mass_evacuated,RFMT,.FALSE. ) // CRLF//&
        " - Total N of food items provided    : " //                          &
              TOSTR(OutData%food_items_encountered,IFMT,.FALSE.) // CRLF //   &
        " - Ingestion ratio ingested/provided : " //                          &
              TOSTR(ratio_ingested,RFMT,.FALSE.) // CRLF

    end associate

  end function model_output_stats_txt

  !> Produce a text string containing general model output statistics.
  !! @note Note that this procedure outputs CSV-formatted data string for
  !!       saving in CSV output file. It does not write the csv output
  !!       file.
  function model_output_stats_row_csv(show_header) result (txt_out)
    use CSV_IO, only : CSV_RECORD_APPEND
    use BASE_UTILS, only : TIMESTAMP_FULL
    !> Optional flag indicating that variable names header is included as
    !! the first line of the string
    logical, optional, intent(in) :: show_header
    character(len=:), allocatable :: txt_out
    character(len=:), allocatable :: csv_record, header_record
    real(SRP) :: ratio_ingested, ratio_fcr
    logical :: show_header_loc
    character(len=LABEL_LEN), dimension(:),                                   &
                            allocatable :: col_time, col_par, col_out, columns
    integer :: i
    character(len=LABEL_LEN) :: timestamp_out
    logical, parameter :: QTE = .FALSE.

    timestamp_out = TIMESTAMP_FULL()

    col_time = [ character(len=45)     :: "TIMESTAMP              " ]     ! 1

    col_par = [ character(len=LABEL_LEN) ::                                   &
                                          !Input parameters:
                                          !123456789012345678901234
                                          "run_model_hours        ",  &   ! 1
                                          "daytime_hours          ",  &   ! 2
                                          "temperature            ",  &   ! 3
                                          "body_mass              ",  &   ! 4
                                          "stomach_capacity       ",  &   ! 5
                                          "midgut_capacity        ",  &   ! 6
                                          "absorption_ratio       ",  &   ! 7
                                          "ingestion_delay        ",  &   ! 8
                                          "water_uptake           ",  &   ! 9
                                          "water_uptake_a         ",  &   !10
                                          "water_uptake_r         ",  &   !11
                                          "appetite_factor_a      ",  &   !12
                                          "appetite_factor_r      ",  &   !13
                                          "appetite_threshold_stom",  &   !14
                                          "digestion_delay        ",  &   !15
                                          "midgut_maxdur          ",  &   !16
                                          "appetite_energy_rate   ",  &   !17
                                          "appetite_energy_shift  ",  &   !18
                                          "midgut_michaelis_r_max ",  &   !19
                                          "midgut_michaelis_k     ",  &   !20
                                          "food_item_mass         ",  &   !21
                                          "food_input_rate        ",  &   !22
                                          "feed_start_offset      ",  &   !23
                                          ! Array-based parameters
                                        [("transport_pattern_t_" //   &   ! 1
                                          TOSTR(i,10),i=1,                   &
                                          size(Global_Transport_Pattern_T))],&
                                        [("transport_pattern_r_" //   &   ! 2
                                          TOSTR(i,10),i=1,                   &
                                          size(Global_Transport_Pattern_R))],&
                                        [("smr_oxygen_temp_" //       &   ! 3
                                          TOSTR(i,10),i=1,                   &
                                          size(Global_Oxygen_Grid_X_Temp))], &
                                        [("smr_oxygen_o2_" //         &   ! 4
                                          TOSTR(i,10),i=1,                   &
                                          size(Global_Oxygen_Grid_Y_O2std))]  ]


    col_out = [ character(len=LABEL_LEN) ::                                   &
                                          ! Output variables
                                          "INGESTED               ",  &   ! 1
                                          "MASS_INGESTED          ",  &   ! 2
                                          "TOTAL_MASS_INGESTED    ",  &   ! 3
                                          "ABSORP_CUMUL           ",  &   ! 4
                                          "ENERGY_FOOD            ",  &   ! 5
                                          "ENERGY_BUDGET          ",  &   ! 6
                                          "BODY_MASS_START        ",  &   ! 7
                                          "BODY_MASS_END          ",  &   ! 8
                                          "FEED_CONVERSION_RATIO  ",  &   ! 9
                                          "N_FOOD_PROVIDED        ",  &   !10
                                          "INGESTION_RATIO        ",  &   !11
                                          "EVACUATION             " ]     !12

    if (Global_Output_Stats_Is_Long) then
      columns = [col_time, col_par, col_out]
    else
      columns = col_out
    end if

    if (present(show_header)) then
      show_header_loc = show_header
    else
      show_header_loc = .FALSE.
    end if
    csv_record = repeat( " ", size(columns) * len(columns(1)) )
    header_record = csv_record

    associate ( OutData => Output_Arrays(size(Output_Arrays)) )

      ratio_ingested = real(OutData%food_items_ingested_total,SRP) /          &
                       real(OutData%food_items_encountered,SRP)

      ! calculate FCR for output F/(M-M0)
      ratio_fcr = FCR(OutData%food_items_ingested_total*Global_Food_Item_Mass,&
                      OutData%body_mass_dynamics -                            &
                                            Output_Arrays(1)%body_mass_dynamics)

      call CSV_RECORD_APPEND(header_record, columns)                            ! 0

      if (Global_Output_Stats_Is_Long) then
        ! Input parameters
        call CSV_RECORD_APPEND(csv_record, timestamp_out )                      ! 0

        call CSV_RECORD_APPEND(csv_record, Global_Run_Model_Hours)              ! 1
        call CSV_RECORD_APPEND(csv_record, Global_Hours_Daytime_Feeding)        ! 2
        call CSV_RECORD_APPEND(csv_record, Global_Temperature)                  ! 3
        call CSV_RECORD_APPEND(csv_record, Global_Body_Mass)                    ! 4
        call CSV_RECORD_APPEND(csv_record, Global_Stomach_Mass)                 ! 5
        call CSV_RECORD_APPEND(csv_record, Global_MidGut_Mass)                  ! 6
        call CSV_RECORD_APPEND(csv_record, Global_Absorption_Ratio)             ! 7
        call CSV_RECORD_APPEND(csv_record, Global_Ingestion_Delay_Min)          ! 8
        call CSV_RECORD_APPEND(csv_record, Global_Water_Uptake)                 ! 9
        call CSV_RECORD_APPEND(csv_record, Global_Water_Uptake_A)               ! 10
        call CSV_RECORD_APPEND(csv_record, Global_Water_Uptake_R)               ! 11
        call CSV_RECORD_APPEND(csv_record, Global_Appetite_Logist_A)            ! 12
        call CSV_RECORD_APPEND(csv_record, Global_Appetite_Logist_R)            ! 13
        call CSV_RECORD_APPEND(csv_record, Global_Appetite_Stomach_Threshold)   ! 14
        call CSV_RECORD_APPEND(csv_record, Global_Digestion_Delay_Min)          ! 15
        call CSV_RECORD_APPEND(csv_record, Global_Maximum_Duration_Midgut_Min)  ! 16
        call CSV_RECORD_APPEND(csv_record, Global_Energy_Appetite_Rate)         ! 17
        call CSV_RECORD_APPEND(csv_record, Global_Energy_Appetite_Shift)        ! 18
        call CSV_RECORD_APPEND(csv_record, Global_Mid_Gut_MM_r_max)             ! 19
        call CSV_RECORD_APPEND(csv_record, Global_Mid_Gut_MM_K_M)               ! 20
        call CSV_RECORD_APPEND(csv_record, Global_Food_Item_Mass)               ! 21
        call CSV_RECORD_APPEND(csv_record, Global_Food_Input_Rate)              ! 22
        call CSV_RECORD_APPEND(csv_record, Global_Run_Model_Feed_Offset)        ! 23
        ! Array-based parameters
        call CSV_RECORD_APPEND(csv_record, Global_Transport_Pattern_T)          ! 1
        call CSV_RECORD_APPEND(csv_record, Global_Transport_Pattern_R)          ! 2
        call CSV_RECORD_APPEND(csv_record, Global_Oxygen_Grid_X_Temp)           ! 3
        call CSV_RECORD_APPEND(csv_record, Global_Oxygen_Grid_Y_O2std)          ! 4
      end if

      ! Output variables
      call CSV_RECORD_APPEND(csv_record, OutData%food_items_ingested_total)     ! 1
      call CSV_RECORD_APPEND(csv_record, OutData%food_items_ingested_total*   & ! 2
                                                         Global_Food_Item_Mass)
      call CSV_RECORD_APPEND(csv_record, Output_Stats%total_mass_food_ingested) ! 3
      call CSV_RECORD_APPEND(csv_record, OutData%food_mass_absorb_cumulate)     ! 4
      call CSV_RECORD_APPEND(csv_record,                                      &
                                feed_energy(OutData%food_mass_absorb_cumulate)) ! 5
      call CSV_RECORD_APPEND(csv_record, OutData%energy_balance_total)          ! 6
      call CSV_RECORD_APPEND(csv_record, Output_Arrays(1)%body_mass_dynamics)   ! 7
      call CSV_RECORD_APPEND(csv_record, OutData%body_mass_dynamics)            ! 8
      call CSV_RECORD_APPEND(csv_record, ratio_fcr)                             ! 9
      call CSV_RECORD_APPEND(csv_record, OutData%food_items_encountered)        !10
      call CSV_RECORD_APPEND(csv_record, ratio_ingested)                        !11
      call CSV_RECORD_APPEND(csv_record, Output_Stats%total_mass_evacuated)     !12

    end associate

    if (show_header_loc) then
      txt_out = header_record // CRLF // csv_record
    else
      txt_out = csv_record
    end if

  end function model_output_stats_row_csv

  ! ============================================================================
  ! ============================================================================

  !> The procedure below is TEMPORARY, for testing and debugging.
  impure subroutine test_run()
    use CSV_IO
    use, intrinsic :: ISO_FORTRAN_ENV, only : ERROR_UNIT
    use BASE_RANDOM, only : RANDOM_SEED_INIT

    !> Maximum number of food items to show, this should never be greater than
    !! commondata::max_food_items_index.
    integer, parameter :: MAX_COL = min(50, MAX_FOOD_ITEMS_INDEX)

    integer(LONG) :: total_steps

    type(FOOD_ITEM) :: feed

    type(FISH) :: fish_agent

    logical :: is_given_feed, is_eaten

    ! Flag for extended/detailed debug logging
    logical, parameter :: IS_LOGGING_DEBUG = IS_EXTENDED_LOGGING_DEBUG_TEST

    ! File name for saving extended/detailed debug data.
    character(len=*), parameter :: DETAILED_CSV_FILE = "debug_data_steps.csv"

    ! CSV file handle
    type(csv_file) :: detailed_csv_output_handle
    integer :: detailed_csv_n_cols

    ! defines if the progress bar is hidden after completion.
    ! @note Note on Intel `ifort`: Intel Fortran optimizes screen writing
    !       so that the progress bar is not updated at each step but rather
    !       writes out in one piece after model completion.
    logical, parameter :: PROGBAR_HIDE = .TRUE.

    ! These four variables keep the previous and next value of the stomach
    ! and midgut capacity, if
    ! commondata::global_stomach_midgut_mass_is_automatic is TRUE
    ! @note Note that these values are necessary to ensure the stomach
    !       and midgut capacity do not shrink if the fish mass is reduced
    !       as aresult of starvation or poor feeding.
    real(SRP) :: mass_stomach_prev_no_shrink, mass_midgut_prev_no_shrink,     &
                 mass_stomach_next_no_shrink, mass_midgut_next_no_shrink

    !- - - - - - - -

    if (.NOT. IS_DEBUG) call RANDOM_SEED_INIT()

    total_steps = total_timesteps( Global_Run_Model_Hours )

    call init_provisioning( total_steps )

    call output_arrays_init( total_steps )

    call feed%init( mass=Global_Food_Item_Mass )

    call fish_agent%new( id_num = 1 )
    if (Global_Stomach_Midgut_Mass_Is_Automatic) then
      call fish_agent%st_init(mass=salmon_stomach_capacity(Global_Body_Mass))
      call fish_agent%mg_init(mass=salmon_midgut_capacity(Global_Body_Mass))
    else
      call fish_agent%st_init( mass=Global_Stomach_Mass )
      call fish_agent%mg_init( mass=Global_MidGut_Mass )
    end if

    DEBUG_REPORT_INIT: if (IS_DEBUG .and. Verbose) then
      write(*,"(a,f9.3)") "DEBUG_REPORT_INIT: Initialized body_mass:",        &
                           fish_agent%mass()
      write(*,"(a,f9.3)") "DEBUG_REPORT_INIT: Initialized stomach_capacity:", &
                           fish_agent%mass_stomach
      write(*,"(a,f9.3)") "DEBUG_REPORT_INIT: Initialized midgut_capacity:",  &
                           fish_agent%mass_midgut
    end if DEBUG_REPORT_INIT

    if ( IS_LOGGING_DEBUG )                                                   &
      call detailed_csv_output_open( DETAILED_CSV_FILE,                       &
                                     detailed_csv_output_handle,              &
                                     detailed_csv_n_cols )

    TIME_LOOP: do Global_Time_Step = 1, total_steps

      DEBUG_WARN_SIZE: if (IS_DEBUG) then
        if (fish_agent%mass() < fish_agent%body_mass0/2.0_SRP)                &
          write(*,*) "DEBUG_WARN_SIZE: Body mass is less than 1/2 of start mass"
        if (fish_agent%mass() < 10.0_SRP)                                    &
          write(*,*) "DEBUG_WARN_SIZE: Body mass too small: ", fish_agent%mass()
      end if DEBUG_WARN_SIZE

      !> - Provide and schedule a food item
      !> - The fish decides to eat the food item scheduled
      call fish_agent%decide_eat( feed%schedule(is_provided=is_given_feed),   &
                                  have_eaten=is_eaten )

      !> - Update one time step of the fish stomach and mid-gut
      call fish_agent%st_step()
      call fish_agent%mg_step()
      call fish_agent%energy_update()
      call fish_agent%do_grow()

      ! This block increases the stomach and midgut capacity, but only
      ! they do not shrink. In case the fish reduces the body mass (e.g.
      ! as a result of starvation), the stomach and gut capacity are
      ! kept constant.
      GROW_STOMACH_MIDGUT: if (Global_Stomach_Midgut_Mass_Is_Automatic) then
        ! previous values
        mass_stomach_prev_no_shrink = fish_agent%mass_stomach
        mass_midgut_prev_no_shrink = fish_agent%mass_midgut
        ! new values calculated from updated body mass
        mass_stomach_next_no_shrink = salmon_stomach_capacity(fish_agent%mass())
        mass_midgut_next_no_shrink = salmon_midgut_capacity(fish_agent%mass())
        if (mass_stomach_next_no_shrink >= mass_stomach_prev_no_shrink)       &
                        fish_agent%mass_stomach = mass_stomach_next_no_shrink
        if (mass_midgut_next_no_shrink >= mass_midgut_prev_no_shrink)         &
                        fish_agent%mass_midgut = mass_midgut_next_no_shrink
      end if GROW_STOMACH_MIDGUT

      !> - Update the output arrays, use ::output_arrays_update_step().
      call output_arrays_update_step( fish_agent )

      !> - Update the total count of food items eaten and not eaten
      call output_arrays_add_ingested( is_given_feed, is_eaten )

      DEBUG_ABSORP_CUMULATE_MISMATCH: if ( IS_DEBUG ) then
        block
          real(SRP) :: abs_diff
          ! Tolerance limit for detectable difference
          real(SRP), parameter :: VERYSMALL = epsilon(1.0_SRP) * 10.0_SRP
          abs_diff=abs( fish_agent%history%food_mass_absorb_cum(HISTORY_SIZE)  &
                - Output_Arrays(Global_Time_Step)%food_mass_absorb_cumulate )
          if ( abs_diff > VERYSMALL  ) write(*,*)                             &
                "DEBUG_ABSORP_CUMULATE_MISMATCH: fish object absorption " //  &
                "does not match output arrays, abs.diff =", abs_diff
        end block
      end if DEBUG_ABSORP_CUMULATE_MISMATCH

      if ( IS_LOGGING_DEBUG ) call                                            &
            detailed_csv_output_write_record( detailed_csv_output_handle,     &
                                              detailed_csv_n_cols )

    end do TIME_LOOP

    if ( IS_LOGGING_DEBUG )                                                   &
        call detailed_csv_output_close( detailed_csv_output_handle )

    contains  ! ----------------------------------------------------------------

      !> **Open** the CSV output file with the detailed step-wise data. Note
      !! that these detailed outputs are produced if ::is_logging_debug
      !! parameter is set to TRUE.
      subroutine detailed_csv_output_open( file_name, csv_handle, total_columns )
        character(len=*), intent(in) :: file_name
        type(csv_file), intent(inout) :: csv_handle
        integer, intent(out) :: total_columns

        integer :: i

        ! Columns of the CSV file go in two kinds: single variable and
        ! array variables with numbered columns.

        ! Fifed columns of the first row, single variable names.
        character(len=LABEL_LEN), dimension(*), parameter ::                  &
          COLUMNS_FIX =  [ character(len=LABEL_LEN) ::                        &
                                                    "STEP          ",   &  !  1
                                                    "FEEDING       ",   &  !  2
                                                    "N_INGESTED    ",   &  !  3
                                                    "N_WASTED      ",   &  !  4
                                                    "APPETITE_STOM ",   &  !  5
                                                    "APPETITE_MIDG ",   &  !  6

                                                    "APPETITE_ENERG",   &  !  7

                                                    "APPETITE_FISH ",   &  !  8
                                                    "STOM_FOOD_MASS",   &  !  9
                                                    "STOM_FOOD_REL ",   &  ! 10
                                                    "STOM_FOOD_N   ",   &  ! 11
                                                    "MIDG_FOOD_MASS",   &  ! 12
                                                    "MIDG_FOOD_REL ",   &  ! 13
                                                    "MIDG_MM_RDECR ",   &  ! 14
                                                    "MIDG_FOOD_N   ",   &  ! 15
                                                    "ABSORPED_TOTAL",   &  ! 16
                                                    "ABSORPED_CUMUL" ]     ! 17

        ! Numbered columns of the first row, variable names, their values are
        ! initialised using (implied) loops.
        character(len=LABEL_LEN), dimension(MAX_COL*5)  :: COLUMNS_NUM

        ! `csv_record` is the temporary character string that keeps the
        ! whole record of the file, i.e. the whole row of the spreadsheet table.
        character(len=:), allocatable :: csv_record

        ! Initialise the array variables with numbered columns names.
        COLUMNS_NUM = [ character(len=LABEL_LEN) ::                       &
                          [("STOM_TIME_" // TOSTR(i,10),i=1,MAX_COL)],    & ! 1
                          [("STOM_MASS_" // TOSTR(i,10),i=1,MAX_COL)],    & ! 2
                          [("MIDG_TIME_" // TOSTR(i,10),i=1,MAX_COL)],    & ! 3
                          [("MIDG_MASS_" // TOSTR(i,10),i=1,MAX_COL)],    & ! 4
                          [("ABSORP_" // TOSTR(i,10),i=1,MAX_COL)]     ]    ! 5

        csv_handle%name = file_name

        total_columns = size(COLUMNS_FIX) + size(COLUMNS_NUM)

        ! Open CSV file for writing.
        call CSV_OPEN_WRITE( csv_handle )
        if ( .not. csv_handle%status ) then
          call csv_file_error_report( csv_handle )
          return
        end if

        ! Prepare the character string variable `csv_record` that keeps the
        ! whole record (row) of data in the output CSV data file. The length of
        ! this string should be enough to fit all the record data, otherwise
        ! the record is truncated.
        csv_record = repeat( " ", (size(COLUMNS_FIX)+size(COLUMNS_NUM))       &
                                                            * (LABEL_LEN+4) )

        ! Create and write column header data
        call CSV_RECORD_APPEND( csv_record, [ COLUMNS_FIX, COLUMNS_NUM ] )
        call CSV_RECORD_WRITE ( csv_record, csv_handle )
        if ( .not. csv_handle%status ) then
          call csv_file_error_report( csv_handle )
          call CSV_CLOSE( csv_handle )
          return
        end if

      end subroutine detailed_csv_output_open

      !> **Close** the CSV output file with the detailed step-wise data. Note
      !! that these detailed outputs are produced if ::is_logging_debug
      !! parameter is set to TRUE.
      subroutine detailed_csv_output_close( csv_handle )
        type(csv_file), intent(inout) :: csv_handle

        call CSV_CLOSE( csv_handle )

      end subroutine detailed_csv_output_close

      !> **Write** the step-wise CSV output record with the actual data. Note
      !! that these detailed outputs are produced if ::is_logging_debug
      !! parameter is set to TRUE.
      !! @note Note that this subroutine is called each time step, so that
      !!       a single row of data (CSV file record)) is written each time.
      subroutine detailed_csv_output_write_record( csv_handle, total_columns )
        type(csv_file), intent(inout) :: csv_handle
        integer, intent(in) :: total_columns

        ! `csv_record` is the temporary character string that keeps the whole
        ! record of the file, i.e. the whole row of the spreadsheet table.
        character(len=:), allocatable :: csv_record

        ! the `csv_record` character string variable is produced such
        ! that it can fit the whole record;
        csv_record = repeat(" ",                                            &
          max( CSV_GUESS_RECORD_LENGTH(total_columns+2, 0.0_SRP), LABEL_LEN ) )

        ! Fill the record (row) by adding variables
        ! - Fixed columns
        call CSV_RECORD_APPEND(csv_record, Global_Time_Step)                ! 1
        call CSV_RECORD_APPEND(csv_record,                                & ! 2
                TOSTR(Global_Interval_Food_Pattern(Global_Time_Step)) )
        call CSV_RECORD_APPEND(csv_record,                                & ! 3
                Output_Arrays(Global_Time_Step)%food_items_ingested_total )
        call CSV_RECORD_APPEND(csv_record,                                & ! 4
                Output_Arrays(Global_Time_Step)%food_items_not_ingested_total )
        call CSV_RECORD_APPEND(csv_record, fish_agent%appetite_stomach() )  ! 5
        call CSV_RECORD_APPEND(csv_record, fish_agent%appetite_midgut() )   ! 6

        call CSV_RECORD_APPEND(csv_record, fish_agent%appetite_energy() )   ! 7

        call CSV_RECORD_APPEND(csv_record,                                & ! 8
                Output_Arrays(Global_Time_Step)%fish_appetite )
        call CSV_RECORD_APPEND(csv_record, fish_agent%st_food_mass() )      ! 9

        call CSV_RECORD_APPEND(csv_record, fish_agent%st_food_mass() /    & !10
                                           fish_agent%mass_stomach )

        call CSV_RECORD_APPEND(csv_record, fish_agent%n_food_items_stomach )!11
        call CSV_RECORD_APPEND(csv_record, fish_agent%mg_food_mass() )      !12

        call CSV_RECORD_APPEND(csv_record, fish_agent%mg_food_mass() /    & !13
                                           fish_agent%mass_midgut )
        call CSV_RECORD_APPEND(                                           & !14
                              csv_record,                                    &
                              michaelis_menten(fish_agent%mg_food_mass() /   &
                                                  fish_agent%mass_midgut,    &
                                                  Global_Mid_Gut_MM_r_max,   &
                                                  Global_Mid_Gut_MM_K_M ) )
        call CSV_RECORD_APPEND(csv_record, fish_agent%n_food_items_midgut ) !15
        call CSV_RECORD_APPEND(csv_record,                                & !16
            Output_Arrays(Global_Time_Step)%food_mass_absorb_total_midgut )

        call CSV_RECORD_APPEND(csv_record,                                & !17
            Output_Arrays(Global_Time_Step)%food_mass_absorb_cumulate    )

        ! - Array columns
        call CSV_RECORD_APPEND(csv_record,                                & ! 1
                fish_agent%food_items_stomach(1:MAX_COL)%time_in_process )
        call CSV_RECORD_APPEND(csv_record,                                & ! 2
                fish_agent%food_items_stomach(1:MAX_COL)%mass )
        call CSV_RECORD_APPEND(csv_record,                                & ! 3
                fish_agent%food_items_midgut(1:MAX_COL)%time_in_process )
        call CSV_RECORD_APPEND(csv_record,                                & ! 4
                fish_agent%food_items_midgut(1:MAX_COL)%mass )
        call CSV_RECORD_APPEND(csv_record,                                & ! 5
                fish_agent%food_items_midgut(1:MAX_COL)%absorped )

        ! Write the step-s data record to the disk.
        call CSV_RECORD_WRITE( csv_record, csv_handle )
        if ( .not. csv_handle%status ) then
          call csv_file_error_report( csv_handle )
          call CSV_CLOSE( csv_handle )
          return
        end if

      end subroutine detailed_csv_output_write_record

  end subroutine test_run

  !> Initialise the food provisioning schedule / pattern
  impure subroutine init_provisioning( time_steps )
    !> Optional overall number of time steps of the model
    !! @note Note that this argument has the integer type of the kind
    !!       commondata::long. This might taint default integer values.
    !! @note This procedure cannot be `pure` because it allocates a
    !!       global module-scope array.
    integer(LONG), intent(in), optional :: time_steps

    logical :: is_file_readable

    integer(LONG) :: time_steps_loc

    if (present(time_steps)) then
      time_steps_loc = time_steps
    else
      time_steps_loc = total_timesteps()
    end if

    if ( Global_Food_Pattern_File == "" ) then
      call food_provisioning_pattern_init( time_steps_loc )
    else
      ! Check if the configuration file exists, if not, exit with error.
      inquire( file=Global_Food_Pattern_File, exist=is_file_readable )
      if ( is_file_readable ) then
        if (Global_Food_Pattern_File_Is_Steps) then
          call food_provisioning_get_raw( time_steps_loc,                     &
                                          Global_Food_Pattern_File,           &
                                          column=255, &  ! the last col is used
                        is_single=.not. Global_Food_Pattern_File_Is_Propagate )
        else
          call food_provisioning_get_file ( time_steps_loc,                   &
                                          Global_Food_Pattern_File,           &
                                          column=255, &  ! the last col is used
                        is_single=.not. Global_Food_Pattern_File_Is_Propagate )
        end if
      else
        write(ERROR_UNIT,*) "WARNING: ", "Food provision file ",              &
              trim(Global_Food_Pattern_File), " does not exist, ",            &
              "use `food_provision_pattern`."
        call food_provisioning_pattern_init( time_steps_loc )
      end if
    end if

  end subroutine init_provisioning

  !> Determine the food schedule array for separate display and analysis
  !! The primary role of this function is to plan (produce and display) the
  !! food provisioning pattern before simulation in GUI
  impure function get_food_schedule_display() result (food_items_encountered)
    integer, allocatable, dimension(:) :: food_items_encountered

    type(FOOD_ITEM), target  :: feed_check
    type(FOOD_ITEM), pointer :: feed_pt

    logical :: is_given
    integer(LONG) :: i, isum, istep
    logical :: is_given_feed

    allocate(food_items_encountered(Global_Run_Model_Hours * HOUR))

    food_items_encountered = 0
    isum = 0

    do i = 1, Global_Run_Model_Hours * HOUR
      feed_pt => feed_check%schedule( i, is_provided=is_given_feed )
      if (associated(feed_pt)) isum = isum + 1
      food_items_encountered(i) = isum
    end do

  end function get_food_schedule_display

  !> calculate the array of the consumption ratio, i.e. eaten/provided, so
  !! it is also easy to calculate the waste rate
  function get_ingestion_ratio_waste(rate_unit) result (consumption)
     !> Optional rate parameter, the rate is calculated per this number of
    !! seconds, the default is per minute
    integer, intent(in), optional :: rate_unit
    real(SRP), allocatable, dimension(:) :: consumption

    integer :: rate_unit_loc
    integer(LONG) :: full_max, i, ii

    ! interval data
    integer, allocatable, dimension(:) :: int_ingested, int_provided

    if (present(rate_unit)) then
      rate_unit_loc = rate_unit
    else
      rate_unit_loc = MINUTE
    end if

    full_max = size(Output_Arrays)

    allocate( int_ingested(full_max/rate_unit_loc),                           &
              int_provided(full_max/rate_unit_loc),                           &
              consumption(full_max/rate_unit_loc) )

    int_ingested = 0
    int_provided = 0
    consumption = 0.0_SRP
    ii = 1

    do  i = 1 + rate_unit_loc, full_max, rate_unit_loc
      int_ingested(ii)=Output_Arrays(i)%food_items_ingested_total -           &
                       Output_Arrays(i-rate_unit_loc)%food_items_ingested_total
      int_provided(ii)=Output_Arrays(i)%food_items_encountered -              &
                       Output_Arrays(i-rate_unit_loc)%food_items_encountered
      ii = ii + 1
    end do

    ! Calculate rate ingested / provided
    where(int_provided > 0)
      consumption = real(int_ingested,SRP) / real(int_provided,SRP)
    end where

  end function get_ingestion_ratio_waste


end module SIMULATE
