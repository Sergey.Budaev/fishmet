!> @file main.f90
!! The FishMet Model

! ------------------------------------------------------------------------------
! Copyright notice:
!   Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad
! FishMet model code is distributed according to GNU AGPL v3, see LICENSE.md
! ------------------------------------------------------------------------------

!> This is the main driver program
program main

  use RUNTIME
  implicit none

  !> ### Implementation notes ###
  !> - Initialise the system with runtime::system_init() procedure;
  call system_init()

  !> - Run the model with runtime::system_run_main().
  call system_run_main()

end program main
