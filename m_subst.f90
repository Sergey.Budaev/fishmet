!> @file m_subst.f90
!! The FishMet Model: The sole function of this component is to provide
!! substituted stubs for components that are disabled at compile time.
!! In the full version, it also provide stubs for GUI and CMD interfaces
!! and graphic libraries if they are disabled at compile time.

! ------------------------------------------------------------------------------
! Copyright notice:
!   Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad
! FishMet model code is distributed according to GNU AGPL v3, see LICENSE.md
! ------------------------------------------------------------------------------

!> The FishMet Model: substituted stubs for components that are disabled at
!! compile time.
module STUBS

use, intrinsic :: ISO_FORTRAN_ENV, only : ERROR_UNIT
use REALCALC, only : SRP
use COMMONDATA

contains

  impure subroutine cmd_iface_process_commands( is_exit_program )
     use ISO_C_BINDING, only: C_NULL_CHAR
     !> Logical output flag indicating that the program should be terminated
     !! If FALSE, the program just re-initialises the system,resets parameters
     !! and restarts system. See runtime::system_run_main().
     logical, intent(out) :: is_exit_program

     print *, 'cmd_iface_process_commands'

     is_exit_program = .TRUE.

  end subroutine cmd_iface_process_commands

  !> This procedure prints an error message and exits.
  impure subroutine exit_with_error_cmd(message, no_exit)

    !> The error message that is printed to the standard error device
    character(len=*), intent(in) :: message
    !> Optional parameter that dictates NOT to stop the program and just
    !! print the error message. This may be useful if the program must
    !! check a series of conditions and then exit at the last one.
    logical, optional, intent(in) :: no_exit

    write(ERROR_UNIT,"(a)") "ERROR: " // message

    ! Check if `no_exit` flag is provided and set TRUE and exit without
    ! halting
    if (present(no_exit)) then
      if (no_exit) return
    end if
    stop

  end subroutine exit_with_error_cmd

  !> This procedure prints a short help, command line parameters
  !! and environment variables when the program is started with
  !! -h or --help switch.
  impure subroutine print_help_switches()

    write(*,"(2a)") "HELP for Fish appetite and feeding model, version ",     &
                    SVN_VERSION_GLOBAL
    write(*,"(a)") ""
    write(*,"(a)") " Environment variables:"
    write(*,"(a)") "  `FFA_MODEL_OUTPUT_TAG` defines the model tag that is added"
    write(*,"(a)") "                to all automatically generated files when"
    write(*,"(a)") "                the model is run in unattended mode."
    write(*,"(a)") ""
    write(*,"(a)") " Command line parameters:"
    write(*,"(a)") "   -h --help /h /help       Prints a short help"
    write(*,"(a)") "   -q --quiet /q /quiet     'quiet' mode, only errors and"
    write(*,"(a)") "                            problems are reported"
    write(*,"(a)") ""
    write(*,"(a)") "Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad"
    write(*,"(a)") "FishMet model code is distributed according to GNU AGPL v3"
    write(*,"(a)") ""
    write(*,"(a)") 'THE PROGRAM IS PROVIDED TO YOU "AS IS", WITHOUT ANY WARRANTY.'

  end subroutine print_help_switches

end module STUBS
