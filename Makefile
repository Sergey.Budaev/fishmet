# FishMet

# ------------------------------------------------------------------------------
# Copyright notice:
#   Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad
# FishMet model code is distributed according to GNU AGPL v3, see LICENSE.md
# ------------------------------------------------------------------------------

# Basic parameers
# Program base name
BASENAME=fishmet

# SOURCES includes all the source files in correct dependency order
SOURCES = m_realcalc.f90  m_bas_utils.f90 m_random.f90 m_csv_io.f90   \
          m_strings.f90 m_parsepar.f90 m_common.f90 m_subst.f90       \
          m_env.f90 m_salmon.f90 m_fish.f90 m_simulate.f90 m_runtime.f90

# Sources that use the graphics library
EXCL_SOURCES = m_ifcmd.f90 m_ifgui.f90 m_runtime.f90 m_simulate.f90

# DRIVER is the source file for the main program.
DRIVER_EXE = main.f90

# EXEFILE is the name of the program executable.
EXEFILE = $(BASENAME).exe

OBJ = $(SOURCES:.f90=.$(OBJEXT)) $(DRIVER_EXE:.f90=.$(OBJEXT))

# Defines the list of object files that are used in the two-stage
# build process on the Windows platform
# Note: note that defining fixed explicit list of object files is needed
#       because different versions of GNU Make may not be compatible in
#       how they expand "*.o", so obtaining the list of object files
#       from the filesystem in place can be broken. Notably, it did not
#       work on Windows 10, neither in the simplest form `*.o` nor
#       with wildcard function `$(wildcard *.$(OBJEXT))`.
OBJFILESWINDOWS = $(SOURCES:.f90=.$(OBJEXT)) $(DRIVER_EXE:.f90=.$(OBJEXT))

# Supported Fortran compiler types
GF_FC = gfortran
IF_FC = ifort

# Choose the default compiler type
FC = $(GF_FC)

# Definitions of compiler flags on various combinations of compiler and
# platform. This is done separately for DEBUG and non-DEBUG builds.

# ++ DEBUG build options:
ifdef DEBUG

  GF_FFLAGS_UNIX = -O0 -g -ffpe-trap=zero,invalid,overflow,underflow
  GF_FFLAGS_WINDOWS = $(GF_FFLAGS_UNIX)

  IF_FFLAGS_UNIX = -O0 -gen-interfaces -g -debug all -fpe0 -traceback         \
                   -init=snan -warn -check bounds,pointers,format,uninit

  IF_FFLAGS_WINDOWS = /Qsox /Z7 /optimize:0 /fpe:0 /debug:all /traceback      \
                      /Qinit:snan /gen-interfaces                             \
                      /check:bounds,pointers,format,uninit /warn

# ++ Release, non-debug build options:
else

  GF_FFLAGS_UNIX = -O3 -march=native -funroll-loops -fforce-addr
  GF_FFLAGS_WINDOWS = $(GF_FFLAGS_UNIX)

  IF_FFLAGS_UNIX = -sox -O3 -ipo -fp-model fast=2 -xHost -finline-functions
  IF_FFLAGS_WINDOWS = /Qsox /O3 /Ot /fp:fast=2 /QxHost /Ob2 /warn /fpe:0

endif

# Path (subdirectory) to the unit tests using pfUnit framework.
PFUNIT_PATH = pfunit

# Determine this makefile's path. Be sure to place this BEFORE `include`s
THIS_FILE := $(lastword $(MAKEFILE_LIST))

# Determine the current directory
CWD:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

# Version control string

# Version header file contains a character string parameter containinbg the
# global project version info from version string
VERSION_INC_FILE = version.h

# Version string is  obtained from SVN
VERSION_STRING := $(shell svn info --show-item last-changed-revision)
# But if SVN is not found in path then unknown is set.
#   Note that this might occur On Windows platform when TortoiseSVN does
#   not include command line utilities or these are not in PATH.
ifndef VERSION_STRING
	VERSION_STRING = unknown_version_osv
endif

# Generate version header for SVN
define AUTOGEN_SVN_VERSION_INC_FILE
	$(shell $(ECHO) "! Project version information:" > $(VERSION_INC_FILE))
	$(shell $(ECHO) "character(len=*), parameter, public :: SVN_VERSION_GLOBAL = trim('r$(VERSION_STRING)')" >> $(VERSION_INC_FILE))
endef

# Determine what is the build platform, Windows / non-Windows
#
# A safer way to check platform if uname is not available, ComSpec on Windows
# Note that ComSpec is (may be?) case-sensitive, check with env.exe
#
# Quirk: - Microsoft Windows echo does not require escaping quotes parentheses
#          and other symbols and outputrs them as is into redirected shell
#          output, e.g. in  $(shell echo "bla bla bla" >> file.txt), i.e
#          file.txt gets "bla bla bla". Linux echo, in contrast deletes the
#          quites, which can create portability problems if a piece of code
#          (e.g. include file) is auto-generated in such a way.
#          One way to ensure identical behaviour on all platforms is to use
#          a third-party echo.exe program from Cygwin, GnuWin32 of Linux
#          subsystem on Windows 10.
#        - Windows 10 does not seem to use third-party `echo.exe` (e.g. from
#          GNUwin32 or Linux subsystem if simple echo is referred under shell.
#          This requires redefining ECHO to refer to a specific `exe`
#          explicitly.
ifdef ComSpec
	PLATFORM_TYPE=Windows
	WHICH_CMD=where
	NULLDEV=":NULL"
	ECHO := "$(firstword $(shell $(WHICH_CMD) echo.exe))"
	RM := rm -fr
	MV := move
else
	PLATFORM_TYPE=Unix
	WHICH_CMD=which
	NULLDEV="/dev/null"
	ECHO := echo
	RM := rm -fr
	MV := mv -f
endif

# Determine build options for various supported platforms and compilers

ifeq ($(PLATFORM_TYPE)$(FC),Unixgfortran)
	OBJEXT=o
	FFLAGS:=$(GF_FFLAGS_UNIX)
endif

ifeq ($(PLATFORM_TYPE)$(FC),Windowsgfortran)
	OBJEXT=o
	FFLAGS:=$(GF_FFLAGS_WINDOWS)
endif

ifeq ($(PLATFORM_TYPE)$(FC),Unixifort)
	OBJEXT=o
	FFLAGS:=$(IF_FFLAGS_UNIX)
endif

ifeq ($(PLATFORM_TYPE)$(FC),Windowsifort)
	OBJEXT=obj
	FFLAGS:=$(IF_FFLAGS_WINDOWS)
endif

# Doxygen configuration file within the same directory.
# Note that DOXY_OUTPUT_DIRECTORY is used only for cleaning, actual doxygen
#      directory is set in DOXYCFG config. file.

DOXYCFG = Doxyfile
DOXY_OUTPUT_DIRECTORY = model_docs

# Manpage
MANUALFMT = pdf
MANPAGE = manpage
MANPAGE_OUT = ${BASENAME}.1

# ------------------------------------------------------------------------------
# Targets follow

# Default build is main executable
all: $(EXEFILE)

# Build object files only without the final executable, this
# is needed for unit tests
objects: m_realcalc.f90 m_subst.f90 $(filter-out $(EXCL_SOURCES),$(SOURCES))
	$(info ------------------------------------------------)
	$(info Building: $(filter-out $(EXCL_SOURCES),$(SOURCES)))
	$(info Excluded: $(EXCL_SOURCES))
	$(info DEBUG: $(DEBUG))
	$(info ------------------------------------------------)
	$(FC) $(FFLAGS) -c $?

# Run unit tests. Note that pFunit is non-essential and any build errors are
# ignored in this main Makefile.
.PHONY: tests
tests:
	$(info ------------------------------------------------)
	$(info Running unit tests in $(PFUNIT_PATH))
	$(info ------------------------------------------------)
	-$(MAKE) DEBUG=1 -C $(PFUNIT_PATH)

# Build version header
$(VERSION_INC_FILE): $(SOURCES)
	$(AUTOGEN_SVN_VERSION_INC_FILE)

# Build the main program executable
# Note that the commands are different in Unix/Linux and Windows
$(EXEFILE): $(AUTOGEN_SVN_VERSION_INC_FILE) $(SOURCES) $(THIS_FILE)

ifeq ($(PLATFORM_TYPE),Unix)
	$(FC) $(FFLAGS) -o $(EXEFILE) $(SOURCES) $(DRIVER_EXE)
endif

ifeq ($(PLATFORM_TYPE)$(FC),Windowsgfortran)
	$(FC) $(FFLAGS) -c $(GRAPHMOD) $(SOURCES) $(DRIVER_EXE)
	$(FC) $(OBJFILESWINDOWS) -o $(EXEFILE)
endif

ifeq ($(PLATFORM_TYPE)$(FC),Windowsifort)
	$(FC) $(FFLAGS) /c $(GRAPHMOD) $(SOURCES) $(DRIVER_EXE)
	link /out:$(EXEFILE) $(OBJFILESWINDOWS)
endif

# Cleanup

clean:
	-$(RM) $(wildcard *.mod) $(wildcard *.$(OBJEXT)) $(EXEFILE) \
		$(VERSION_INC_FILE) $(wildcard *.pdb)

distclean: clean clean_plots clean_refs clean_doxy_deps clean_tmp_dirs clean_tests
	-$(RM) $(DOXY_OUTPUT_DIRECTORY) $(wildcard *tmp) $(wildcard zzz*) \
	        $(wildcard *.csv) $(wildcard fort.*)
	-$(RM) ${MANPAGE}.${MANUALFMT} ${MANPAGE}.html ${MANPAGE_OUT}
	-$(RM) $(wildcard *.xml) $(wildcard dox/*.xml)
	-$(RM) *.1
	-$(RM) srv/*pdf srv/*html

clean_tests:
	-$(MAKE) -C $(PFUNIT_PATH) distclean

clean_tmp_dirs:
	-$(RM) bin
	-$(RM) tools/*.exe

clean_plots:
	-$(RM) $(wildcard dox/plot*.svg) $(wildcard *.png) \
	        $(wildcard dox/draw*.svg) $(wildcard dox/uml*.svg) \
	        $(wildcard dox/lo_draw*.svg)

clean_refs:
	-$(RM) *-ref.adoc *-ref.cfg dox/*-ref.adoc dox/*-ref.cfg \
					dox/*-ref.py scripts/*-ref.py

manpage: $(MANPAGE_OUT) ${MANPAGE}.${MANUALFMT}

${MANPAGE_OUT}: ${MANPAGE}.adoc
	a2x -fmanpage $<

${MANPAGE}.${MANUALFMT}: ${MANPAGE}.adoc
	a2x -f${MANUALFMT} $<

${MANPAGE}.html: ${MANPAGE}.adoc
	asciidoc -b html5 -a icons -a toc2 -a theme=flask $<

# Doxygen docs with generated prerequisites

clean_doxy_deps:
	-$(RM) doxy_model_outline.md

doxygen: $(DOXYCFG)
	doxygen $(DOXYCFG)

docs: ${MANPAGE_OUT} ${MANPAGE}.pdf ${MANPAGE}.html doxygen

# Conversion of hand-produced ODG graphics to SVG for inclusion in docs

# Make automatic vars are here:
#    https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html
# - $(@F) = target without path/dir
# - $(<F) = first prerequisite without path/dir

dox/lo_draw-%.svg: dox/lo_draw-%.odg
	cd $(@D) ; $(LIBREOFFICE_EXEC) --draw --headless --convert-to svg:"draw_svg_Export" $(<F)

dox/draw-%.svg: dox/draw-%.dot
	cd $(@D) ; dot -Tsvg $(<F) > $(@F)

dox/uml-%.svg: dox/uml-%.plantuml
	cd $(@D) ; plantuml -tsvg $(<F)

schemes: dox/lo_draw-model-overview.svg dox/draw-modules-dep.svg dox/draw-manual-dep.svg dox/draw-main-comps.svg dox/uml-fish-class-diagram.svg dox/uml-feed-pattern.svg
