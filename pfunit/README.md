# Unit tests

NOTE: This is an example unit test configuration. Tested with pFunit 
      version 3.2.9

The `pfunit` directory contains Makefiles for unit tests of the model using
the pFUnit framework for Fortran.

* pFUnit 3 (old, obsolete) is obtained [here](http://pfunit.sourceforge.net).
* pFUnit 4 is current version at [Github](https://github.com/Goddard-Fortran-Ecosystem/pFUnit)
.

