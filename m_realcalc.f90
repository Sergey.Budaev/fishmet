!> @file m_realcalc.f90
!! The FishMet Model: Definition of precision constants.

! ------------------------------------------------------------------------------
! Copyright notice:
!   Copyright (c) 2015-2024 Sergey Budaev & Ivar Rønnestad
! FishMet model code is distributed according to GNU AGPL v3, see LICENSE.md
! ------------------------------------------------------------------------------

!> Module implementing various general procedures for operating real type
!! variables and numbers:
!!  - Define the precision kind constants and tolerance limits:
!!  - Define functions for checking near-equality of real numbers.
module REALCALC

  implicit none

  !> These constants define single, double and quadruple precision kinds
  !! in a portable way, specified by (a) minimum decimal precision and/or
  !! (b) exponent range. **Use this as a template.**
  integer, parameter, public :: S_PREC_32  = selected_real_kind( 6,   37)
  integer, parameter, public :: D_PREC_64  = selected_real_kind(15,  307)
  integer, parameter, public :: Q_PREC_128 = selected_real_kind(33, 4931)

  !> This constant defines big integer
  integer, parameter, public :: LONG = selected_int_kind(16)

  !> Standard precision parameter. This parameter defines the real type
  !! kind for use in all other parts of the code.
  !!  - **SRP** stands for Standard Real Precision
  integer, parameter, public :: SRP = D_PREC_64

  !> Definition of the **high** real precision (**HRP**).  This real type kind
  !! is used in pieces where a higher level of FPU precision is required, e.g.
  !! to avoid overflow/underflow and similar errors.
  integer, parameter, public :: HRP = Q_PREC_128

  !> This is just zero, its use makes it easier to avoid caveats and errors
  !! with literal constants, e.g. placing 0.0 (that assumes the default kind)
  !! in a function that accepts real(SRP) argument:
  !! func(0.0) might result in error while func(NIL) is always correct.
  real(SRP), parameter, public :: NIL = 0.0_SRP

  !> Missing data code. A real type code that is placed if a value is missing
  !! for some reason. Using a weird number like -9999.0 makes it easier to
  !! detect wrong data usage because this large negative value is likely to
  !! propagate in calculations and be easily detectable. All numbers should
  !! be ideally initialised to m_realcalc::missing.
  real(SRP), parameter, public :: MISSING = -9999.0_SRP

  !> Numerical code for invalid or *missing* **integer** counts.
  !! @note It is safe to assign integer `UNKNOWN` to a **real** type variable,
  !!       e.g. `real(SRP) :: value=UNKNOWN`.
  integer, parameter, public :: UNKNOWN = -9999

  !> Tolerance parameter. It is defined as  **twice** the smallest `real`
  !! number *E* such that @f$ 1 + E > 1 @f$.
  real(SRP), parameter, public :: SMALL = epsilon(0.0_SRP) * 2.0_SRP

  !> Float equal operator. tests if two real values are equal with some small
  !! tolerance equal to m_realcalc::small.
  !!
  !! Usage:
  !! @code
  !!   if ( a .feq. b ) ...
  !! @endcode
  !! is roughly equivalent to
  !! @code
  !!   if ( a == b ) ...
  !! @endcode
  !!
  !! @warning Some compilers may still not support newer Fortran standard
  !!          and not allow user-defined operators. Oracle f95 v. 12.6 for
  !!          example does not support this.
  interface operator (.feq.)
    procedure float_equal_op_feq
  end interface operator (.feq.)

  private float_equal_op_feq

  !> Calculate an average of an array excluding missing code values.
  !! @note The code of `average` function is copied from the AHA model **M1**
  !!       r8510 m_common.f90
  interface average
    module procedure average_r
    module procedure average_i
  end interface average

  private :: average_r, average_i


contains

  !> Check if two real values are nearly equal using the
  !! Thus function can be used for comparing two real values like the below.
  !! The exact comparison (incorrect due to possible rounding):
  !! @code
  !!   if ( a == b ) ...
  !! @endcode
  !! should be substituted by such comparison:
  !! @code
  !!   if ( float_equal(a, b, 0.00001) ) ...
  !! @endcode
  elemental function float_equal(value1, value2, epsilon) result (equal)
    !> The first value for comparison.
    real(SRP), intent(in) :: value1
    !> The second value for comparison.
    real(SRP), intent(in) :: value2
    !> optional (very small) tolerance value.
    real(SRP), intent (in), optional :: epsilon
    !> TRUE if the values `value1` and `value2` are nearly equal.
    logical :: equal

    real(kind(epsilon)) :: local_epsilon
    local_epsilon = SMALL

    if ( present(epsilon) ) then
      local_epsilon = abs(epsilon)
    end if

    equal = abs( value1-value2 ) <= local_epsilon

  end function float_equal

  !> A simplified version of m_realcalc::float_equal() for use in the `.feq.`
  !! operator.
  elemental function float_equal_op_feq(value1, value2) result (equal)
    !> The first value for comparison.
    real(SRP), intent(in) :: value1
    !> The second value for comparison.
    real(SRP), intent(in) :: value2
    !> TRUE if the values `value1` and `value2` are nearly equal.
    logical :: equal

    equal = abs( value1-value2 ) <= SMALL

  end function float_equal_op_feq

  !> Rescale input value X from 0:1 to arbitrary range A:B.
  !! Example call:
  !! @code
  !! X = rescale10( X, rmin, rmax )
  !! X = rescale10( X, 10.0_SRP, 1000.0_SRP )
  !! @endcode
  elemental function rescale10(X, A, B) result (RX)
    !> Input value within 0:1
    real(SRP), intent(in)  :: X
    !> Rescale range, lower
    real(SRP), intent(in)  :: A
    !> Rescale range, upper
    real(SRP), intent(in)  :: B
    real(SRP) :: RX

    RX = X * (B-A) + A

  end function rescale10

  !-----------------------------------------------------------------------------
  !> Calculate an average value of a real array, excluding MISSING values.
  !! @returns The mean value of the vector.
  !! @note This is a **real** array version.
  pure function average_r (array_in, missing_code, undef_ret_null)            &
                                                              result (mean_val)

    !> The input data vector.
    real(SRP), dimension(:), intent(in) :: array_in

    !> Optional parameter setting the missing data code,
    !! to be excluded from the calculation of the mean.
    real(SRP), optional, intent(in) :: missing_code

    !> Optional parameter, if TRUE, the function returns zero rather than
    !! undefined if the sample size is zero.
    logical, optional, intent(in) :: undef_ret_null

    ! The mean value of the vector.
    real(SRP) :: mean_val

    ! Local missing code.
    real(SRP) :: missing_code_here

    ! Local sample size, N of valid values.
    integer :: count_valid

    ! Big arrays can result in huge sum values, to accommodate them,
    ! use commondata::hrp and commondata::long types
    real(HRP) :: bigsum, bigmean

    !> ### Implementation details ###

    !> Check if missing data code is provided from dummy input.
    !! If not, use global parameter.
    if (present(missing_code)) then
      missing_code_here = missing_code
    else
      missing_code_here = MISSING
    end if

    !> Fist, count how many valid values are there in the array.
    count_valid = count(array_in /= missing_code_here)

    !> If there are no valid values in the array, mean is undefined.
    if (count_valid==0) then
      if (present(undef_ret_null)) then
        if (undef_ret_null) then
          mean_val = 0.0_SRP    !> still return zero if undef_ret_null is TRUE.
        else
          mean_val = MISSING
        end if
      else
        mean_val = MISSING
      end if
      return
    end if

    bigsum = sum( real(array_in, HRP), array_in /= missing_code_here )
    bigmean = bigsum / count_valid

    mean_val =  real(bigmean, SRP)

  end function average_r

  !-----------------------------------------------------------------------------
  !> Calculate an average value of an integer array, excluding MISSING values.
  !! @returns The mean value of the vector
  !! @note This is an **integer** array version.
  pure function average_i (array_in, missing_code, undef_ret_null)            &
                                                              result (mean_val)

    !> The input data vector.
    integer, dimension(:), intent(in) :: array_in

    !> Optional parameter setting the missing data code,
    !! to be excluded from the calculation of the mean.
    integer, optional, intent(in) :: missing_code

    !> Optional parameter, if TRUE, the function returns
    !! zero rather than undefined if the sample size is zero.
    logical, optional, intent(in) :: undef_ret_null

    ! @returns The mean value of the vector
    real(SRP) :: mean_val

    ! Local missing code
    integer :: missing_code_here

    ! Local sample size, N of valid values.
    integer :: count_valid

    ! Big arrays can result in huge sum values, to accommodate them,
    ! use commondata::hrp and commondata::long types
    integer(LONG) :: bigsum
    real(HRP) :: bigmean

    !> ### Implementation details ###

    !> Check if missing data code is provided from dummy input.
    !! If not, use global parameter.
    if (present(missing_code)) then
      missing_code_here = missing_code
    else
      missing_code_here = UNKNOWN
    end if

    !> Fist, count how many valid values are there in the array.
    count_valid = count(array_in /= missing_code_here)

    !> If there are no valid values in the array, mean is undefined.
    if (count_valid==0) then
      if (present(undef_ret_null)) then
        if (undef_ret_null) then
          mean_val = 0.0_SRP    !> still return zero if undef_ret_null is TRUE.
        else
          mean_val = MISSING
        end if
      else
        mean_val = MISSING
      end if
      return
    end if

    bigsum = sum( int(array_in, LONG), array_in /= missing_code_here )
    bigmean = real(bigsum, HRP) / real(count_valid, HRP)

    mean_val = real(bigmean, SRP)

  end function average_i

end module REALCALC
