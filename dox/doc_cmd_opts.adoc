
*-h, --help, /h or /help*::
    print brief help
    indexterm:[command line options,-h]
    indexterm:[command line options,--help]

*-q, --quiet, /q, /quiet*::
    "quiet mode" with minimum output.
    indexterm:[verbose mode]
    indexterm:[quiet mode]
    indexterm:[command line options,-q]
    indexterm:[command line options,--quiet]

